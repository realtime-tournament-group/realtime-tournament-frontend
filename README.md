# RealtimeTournamentFrontend

<img src="./logo.png"  />

## Содержание

1. Описание проекта
2. Сборка общего проекта
3. ### Frontend
  
   1. Инструментарий
   2. Реализованные фичи
   3. Запланированные фичи
   4. Сложности
   5. Рефлексия по проекту
4. ### Backend
  
   2. Инструментарий
   2. Реализованные функции
   3. Запланированные функции
   4. Сложности
   5. Ошибки
   6. Архитектура

----


### Описание проекта

Данное приложение реализует возможности проведения турнира для разных людей у которых нет много времени на составление плана, сборки участников в определенное время, также необходимо контроллировать участников турнира которые собираются покинуть турнир, поскольку в таком случае необходимо полностью перестраивать турнирную сетку и перераспределять игроков по разным матчам

### Сборка общего проекта Frontend+ Backend

Для удобства был сформирован config в **docker-compose.yaml**

Данный файл находится в backend репозитории поэтому можно склонировать репозиторий.

Для запуска контейнеров необходимо ввести 

```bash
docker-compose build && docker-compose up
```

[https://gitlab.com/realtime-tournament-group/realtime-tournament-backend-node-nest](ссылка на backend)

```yaml
version: '3.7'
services:
  postgres-db:
    restart: always
    image: postgres:latest
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DATABASE: realtime
    ports:
      - "5432:5432"
  backend:
    restart: always
    build:
      context: ./
      dockerfile: Dockerfile
    command:   yarn start
    ports:
      - "4000:4000"
      - "4500:4500"
      - "4600:4600"
    volumes:
      - "./dev/realtime-tournament-backend-node-nest:/realtime-tournament-backend-node-nest"
    depends_on:
      - postgres-db
```



-----

###  Frontend

#### Инструментарий

- Транслятор Javascript **Typescript**
- Проверка кода  **TSLint** 
-  Абстракция для CSS **SCSS**
- Библиотека работа с событиями **RxJS**
- Работа с Dom элементами  **React**
- Менеджер состояний приложения **Redux Observable**
- Роутинг **React Router Dom**
- Абстракция для IndexedDb **Idb**
- Генерация QrCode **ReactQrCode**
- Веб-сокет клиент **Socket-io-Client**
- Окружение для Nodejs  **CrossEnv**
- Http Client входящий в состав RxJS  **RxAjax** 
- Тестирование **TsJest**
- горячая перегрузка компонентов **React-hot-loader**
- Сборка проекта **Webpack** 
- Непрерывная интеграция **GitlabCI**

#### Реализованные фичи

1. Создание турнира 
2. Возможность поделится турниром через  QrCode и соц сети
3. Подключение к турниру с выбором из списка
4. Авторизация создателя турнира через Хэши
5. Авторизация участника турнира путем сложения хэшей
6. Турнир
   1. Запуск турнира
   2. Постановка в очередь для игроков турнира
   3. Чат панель
   4. Список игроков
   5. Турнирная сетка с обновлениями в реальном времени
   6. Турнирная таблица
   7. Выбор матча для установки счета игрокам турнира

####  Запланированные фичи

1. Реализация WebRTC стрима для онлайн игр

2. Личные аккаунты пользователей

3. Авторизация и ауентификация пользователей через BlockChain

4. Участие в нескольких турнирах одновременно

5. Возможность генерация пустой сетки c экспортом в PNG

6. Собственная   библиотека компонентов

7. Турнир

   1. Остановка турнира
   2. Подарки для игроков 
   3. Настройки
      1. Возможность назначать судей
      2. Устанавливать изображение для турнира

8. Тестирование интерфейса через CyPress

9. Перенос турнирной сетки в Canvas для увеличения производительности

   

   ####  Сложности

   1. Работа с новым инструментарием RxJS и менеджером состояний Redux Observable
      1. При работе c Websocket клиентом socket-io-client есть проблемы в передачи событий в Action, обработка событий происходила с помощью RxJS, но при попытке собрать все события в один поток библиотека ругалась на проблему в actions, поскольку они должны быть объектами, а не асинхронными функциями
      2. После отправки данных с сервера через socket, некоторые события не обрабатывались, поскольку rxjs уже закрыл поток выполнения либо он еще не начал слушать событие. Поэтому необходимо реализовать стандартный  ws протокол и использовать rxWebsockets в RxJS
      3. Подключение к нескольким socket room приводила к ошибке библиотеки и закрывала все активные соеденения в сокете
      4. При обработки исключительных ситуаций при запросах на сервер rxjs единоразово обрабатывал ошибку и больше не давал возможности отправлять запросы на сервер
      5. При обновления состояния некоторых компонентов они не обновлялись, даже в случае явного указания на обновления, данные действия не выполнялись
   2. Работа с формами, для обработки ввода пользователя использовались состояние компонента, что является не лучшим вариантом и лучше использовать Redux Form поскольку данная библиотека позволяет более эффективно обрабатывать логику с форм, но о существовании этой библиотеки узнал слишком поздно
   3. Тестирование Rxjs, при попытке протестировать actions, появлялись проблемы с подпиской на события хотя, данное действие не сомнено выполнялось
   4. При работе с турнирой сеткой были использованы стандартные методы отображения линий матчей для перехода по раундам и при большом количестве игроков (не четном) игровая сетка ломалась и было не понятно какую следующий матч должен выполнятся. Также при работе с большим количеством игроков появились проблемы с производительностью (в доработках указан метод решения данной проблемы)
   5. CORS - http клиент в rxjs при подаче конфига с headers для запросов не позволял выполнять запросы на сторонние домены, решилось путем настройки proxy в webpack, но это не панацея поскольку архитектура проекта готова к распределенной микросервисной архитектуре  и таком случае подключение к другим портам объявит данную проблему. Проблема оказалась в библиотеке rxjs/ajax/dom.

   #### Рефлексия по проекту

   При выполнии данного проекта был получен бесценный опыт во всех этапах разработки проекта и были выявлены некоторые критерии к разработке реальных\учебных проектов. 

   ​	Основное правило  при разработке это установка четких требований к проекту поскольку от данного действия определяется успех проекта и на при мере данного проекта я убедился в этом поскольку, в начале проекта я не установил конкретные функции которые собирался реализовать, в следствие этого было распыление на все функции проекта что привело к проблемам недоработкам и упущеннуму времени.

   ​	При разработке проекта лучше использовать известные технологии поскольку они снизили бы время на разработку приложения и позволили избежать множество проблем. Новый инструментарий лучше основывать в тестовых приложениях.

   

   ----

   ### Backend

   #### Инструментарий

   1. Транслятор Javascript **Typescript**
   2. Проверка кода  **TSLint**
   3. Веб фреймворк для Nodejs **Express**
   4. Абстракция над Express **Nest**
   5. База данных **PostgreSQL**
   6. ORM библиотека **TypeORM**
   7. Веб-сокет на основе socket.io **NestWebsocketGateway**
   8. Сборка проекта **Webpack**
   9. Тестирование **TsJest**
   10. Swagger **OpenApi**
   11. Документация **Compodoc**
   12. Виртуализатор программной среды  **Docker**
   13. Оркестратор Докер-контейнеров **DockerCompose**
   14.  Непрерывная интеграция **GitlabCI**

   #### Реализованные функции

   1. Сервисы и контроллеры для работы с данными
   2. WebsocketGateways для чата и турнира
   3. Сервис авторизация через JWT
   4. Модульный проект с готовностью к Микросервисной архитектуре

   #### Запланированные функции

   1. Смена оркестратора контейнеров на Kubernetes
   2. Распределить модули приложения по микросервисам
   3. Модуль для работы с WebRTC
   4. Deploy проекта в Amazon Fargate
   5. AWS Lambda для удаления простаивающих турниров из базы данных

   #### Проблемы

   1. При работе с большим количеством подключений к серверу, сервер падал с ошибкой переполнения jsHeap, поэтому необходимо все запросы выполнять через Websocket чтобы уменьшить запросы на сервер

      ![Screen Shot 2019-09-14 at 10.37.39](./Screen Shot 2019-09-14 at 10.37.39.png)

   2. При работе с Websocket не получилось внедрить зависимости из других сервисов приложения, поэтому было решено использовать запросы через Http

   3. При тестиривании с большим количеством игроков, в некоторых случаях выдавало ошибку  **sql_error_code = 54000 program_limit_exceeded**

   #### Архитектура проекта

   ![Screen Shot 2019-09-15 at 15.09.42](./Screen Shot 2019-09-15 at 15.09.42.png)



# Краткий гайд по проекту

### Создание турнира

Можно ввести наименование турнира и выбрать опции 

![Screen Shot 2019-09-15 at 18.44.12](./Screen Shot 2019-09-15 at 18.44.12.png)

После создания турнира пользователь администратор может поделится ссылками с другими игроками

![Screen Shot 2019-09-15 at 18.44.20](./Screen Shot 2019-09-15 at 18.44.20.png)

После того как другие игроки были приглашено им будет предложено ввести ник для того авторизироваться и участвовать в турнире

![Screen Shot 2019-09-15 at 18.45.09](./Screen Shot 2019-09-15 at 18.45.09.png)

После авторизации пользователь может встать в очередь нажав на красную кнопку в левой части экрана.

![Screen Shot 2019-09-15 at 18.45.20](./Screen Shot 2019-09-15 at 18.45.20.png)

После того как количество людей в очереди будет 2 или больше, сервер автоматических создает матч в котором эти игроки участвуют

![Screen Shot 2019-09-15 at 18.50.22](./Screen Shot 2019-09-15 at 18.50.22.png)

Администратор турнира может выбрать матч и поставить оценку одному из игроков

