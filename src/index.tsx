import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Root from './Root';

const root = document.getElementById('tournament-root');
ReactDOM.render(<Root/>, root);
