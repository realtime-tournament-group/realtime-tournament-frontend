import {combineReducers} from 'redux';
import {combineEpics} from 'redux-observable';
import {adminCheckEpic, adminReducer, setScoreEpic, startTournamentEpic} from './admin';
import {chatConnectEpic, chatReducer, receiveMessageStreamEpic, sendMessageEpic} from './chat';
import {autocompleteEpic, comeInEpic, comeInReducer} from './comeIn.tournament';
import {createTournamentEpic, createTournamentReducer, saveTournament, saveTournamentEpic} from './create.tournament';
import {matchEpic, matchReducer} from './match';
import {
    playerReducer,
    userMatchApplyEpic,
    userMatchCancelEpic,
    userQueueEpic,
    userQueueLeaveEpic
} from './player';
import {playerAuthCheckEpic, playerAuthEpic, playerAuthReducer} from './player.auth';
import {
    tournamentConnectEpic,
    tournamentDisconnectEpic, tournamentLoadEpic,
    tournamentReducer,
    tournamentStreamEpic,
} from './tournament';

export const rootEpic = combineEpics(
    receiveMessageStreamEpic,
    sendMessageEpic,
    chatConnectEpic,
    tournamentConnectEpic,
    tournamentDisconnectEpic,
    tournamentStreamEpic,
    autocompleteEpic,
    createTournamentEpic,
    saveTournamentEpic,
    adminCheckEpic,
    userMatchApplyEpic,
    userMatchCancelEpic,
    userQueueLeaveEpic,
    userQueueEpic,
    startTournamentEpic,
    // setJudgeEpic,
    // removeJudgeEpic,
    comeInEpic,
    playerAuthEpic,
    playerAuthCheckEpic,
    tournamentLoadEpic,
    matchEpic,
    setScoreEpic,
);

export const rootReducer = combineReducers({
    chatReducer,
    createTournamentReducer,
    tournamentReducer,
    adminReducer,
    playerReducer,
    comeInReducer,
    playerAuthReducer,
    matchReducer,
});
