import {ActionsObservable, Epic, ofType} from 'redux-observable';
import {of} from 'rxjs';
import {ajax} from 'rxjs/ajax';
import {catchError, debounceTime, map, mapTo, switchMap, tap} from 'rxjs/operators';
import {BACKEND_URL, HEADERS} from '../api/ajax';
import {db} from '../api/db';
import {projectRoot} from './root';

const app = 'come_in';
const tournamentApiURL = `${BACKEND_URL}/tournament`;

export const FIND = `${projectRoot}/${app}/find`;
export const FIND_SUCCESS = `${projectRoot}/${app}/find/success`;
export const COMEIN_CONNECT = `${projectRoot}/${app}/connect`;
export const COMEIN_SUCCESS = `${projectRoot}/${app}/connect/success`;
export const AUTOCOMPLETE = `${projectRoot}/${app}/autocomplete`;
export const AUTOCOMPLETE_SUCCESS = `${projectRoot}/${app}/autocomplete/success`;

export const autoCompleteRequest = () => ({type: AUTOCOMPLETE});

const autoCompleteSuccess = (data) => ({type: AUTOCOMPLETE_SUCCESS, payload: data});
export const comeInRequest = (name) => ({type: COMEIN_CONNECT, payload: name});
export const comeInSuccess = () => ({type: COMEIN_SUCCESS});

export const autocompleteEpic = (action$) => action$.pipe(
    ofType(AUTOCOMPLETE),
    debounceTime(2000),
    switchMap(() => ajax.getJSON(tournamentApiURL, HEADERS).pipe(
        map(list => autoCompleteSuccess(list))
    ))
);
export const comeInEpic = (action$) => action$.pipe(
    ofType(COMEIN_CONNECT),
    debounceTime(2000),
    switchMap(({payload}) =>
        ajax.post((tournamentApiURL+'/connect'), {name: payload}, HEADERS).pipe(
            map(({response}) => response),
            mapTo(comeInSuccess())
        )),
);

export const comeInReducer = (state = {
    data: [],
    redirect: false
}, action) => {
    switch (action.type) {

        case AUTOCOMPLETE_SUCCESS:
            return {
                ...state,
                data: action.payload
            };
        case COMEIN_SUCCESS:
            return {...state, redirect: true};
        default:
            return state;
    }
};
