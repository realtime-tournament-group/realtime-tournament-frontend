import {any} from 'prop-types';
import {ofType} from 'redux-observable';
import {combineLatest, EMPTY, interval, merge, of, race} from 'rxjs';
import {ajax} from 'rxjs/ajax';
import {debounceTime, map, mapTo, mergeAll, mergeMap, startWith, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {BACKEND_URL} from '../api/ajax';
import {uuid} from '../api/uuid';
import ChatSocket from '../sockets/chat';
import TournamentSocket from '../sockets/tournament';
import {projectRoot} from './root';

const apiURL = `${BACKEND_URL}/tournament`;
const app = 'tournament';
const CONNECT = `${projectRoot}/${app}/connect`;
const UPDATE = `${projectRoot}/${app}/update`;
const LOAD = `${projectRoot}/${app}/load`;
const LOAD_SUCCESS = `${projectRoot}/${app}/load/success`;
const LOAD_USERS_SUCCESS = `${projectRoot}/${app}/load/users/success`;
const LOAD_MATCHES = `${projectRoot}/${app}/load/users`;
const LOAD_MATCHES_SUCCESS =
    `${projectRoot}/${app}/load/users/success`;

const CONNECT_SUCCESS = `${projectRoot}/${app}/connect/success`;
const CONNECT_FAILURE = `${projectRoot}/${app}/connect/failure`;
const DISCONNECT = `${projectRoot}/${app}/disconnect`;
const STREAM_START = `${projectRoot}/${app}/stream/start`;
const STREAM_STOP = `${projectRoot}/${app}/stream/stop`;
const UPDATE_ROUNDES = `${projectRoot}/${app}/update/roundes`;

const PLAYERS_UPDATE = `${projectRoot}/${app}/update/players`;
const PLAYER_ADD = `${projectRoot}/${app}/add/player`;
const PLAYER_REMOVE = `${projectRoot}/${app}/remove/player`;

const MATCHES_UPDATE = `${projectRoot}/${app}/update/matches`;
const MATCHES_ADD = `${projectRoot}/${app}/add/match`;
const MATCH_REMOVE = `${projectRoot}/${app}/remove/match`;


const JUDGES_UPDATE = `${projectRoot}/${app}/judge/update`;
const JUDGE_REMOVE = `${projectRoot}/${app}/judge/remove`;
const JUDGE_ADD = `${projectRoot}/${app}/judge/add`;

const QUEUE_UPDATE = `${projectRoot}/${app}/queue/update`;
const QUEUE_JOIN = `${projectRoot}/${app}/queue/join`;
const QUEUE_LEAVE = `${projectRoot}/${app}/queue/leave`;

const SCORE_SET = `${projectRoot}/${app}/player/set_score`;

export const tournamentConnect = (name) => ({type: CONNECT, name});
const tournamentStreamStart = () => ({type: STREAM_START});
const tournamentUpdateStatus = (status) => ({type: UPDATE, payload: status});

export const updateQueue = () => ({type: QUEUE_UPDATE});
export const tournamentLoad = (name) => ({type: LOAD, payload: name});
// const tournamentUpdateRoundes = (roundes) => ({type: UPDATE_ROUNDES, payload: roundes});
// const tournamentUpdatePlayers = (type, players) => ({type, payload: players});
// const tournamentUpdateJudge = (type, judge) => ({type, payload: judge});
// const tournamentUpdateMatch = (type, player) => ({type, payload: player});
// const tournamentUpdateQueue = (type, player) => ({type, payload: player});
// const tournamentSetScore = (player) => ({type: SCORE_SET, payload: player});
const socket = new TournamentSocket();

export const tournamentConnectEpic = (action$) => action$.pipe(
    ofType(CONNECT),
    tap(({name}) => socket.connect(name)),
    map(({name}) => tournamentLoad(name)),
    //mapTo(tournamentStreamStart()),
);
export const tournamentDisconnectEpic = (action$) => action$.pipe(
    ofType(DISCONNECT),
    tap(() => socket.disconnect()),
    mapTo(() => EMPTY),
);

// export const tournamentStreamEpic = (action$) => action$.pipe(
//     ofType(STREAM_START),
//     debounceTime(5000),
//     switchMap(() => socket.onUpdateTournament().pipe(
//         tap((el) => console.log(el)),
//         map(({status}) => tournamentUpdateStatus(status)),
//     )),
// );

export const tournamentLoadEpic = (action$) => action$.pipe(
    ofType(LOAD),
    tap(({payload}) => console.log(payload)),
    mergeMap(({payload}) => interval(4000).pipe(
        switchMap(() =>
            ajax.getJSON(apiURL + '/find/' + payload)
                .pipe(
                    map(resp => ({type: LOAD_SUCCESS, payload: resp}))
                ))))
);
export const tournamentStreamEpic = (action$) => action$.pipe(
    ofType(STREAM_START),
    debounceTime(5000),
    switchMap(() => socket.onUpdateTournament().pipe(
        tap((el) => console.log(el)),
        map(({status}) => tournamentUpdateStatus(status)),
        ),
    )
);

// export const judgeUpdateStreamEpic = (action$) => action$.pipe(
//     ofType(JUDGES_UPDATE),
//     tap(el => console.log('inited')),
//     mergeMap(() =>
//             socket.onAddJudge().pipe(
//                 map(({id, player}) =>
//                     tournamentUpdateJudge(JUDGE_ADD, {id, player}),
//                 ),
//             ),
//         () => socket.onRemoveJudge().pipe(
//             map(({id, player}) =>
//                 tournamentUpdateJudge(JUDGE_REMOVE, {id, player}),
//             ),
//         ),
//     )
// );

// export const updatePlayerStreamEpic = (action$) => action$.pipe(
//     ofType(PLAYERS_UPDATE),
//     switchMap(() => race(
//         socket.onPlayerJoin().pipe(
//             map(({id, player}) =>
//                 tournamentUpdatePlayers(PLAYER_ADD, {id, player}),
//             ),
//         ),
//         socket.onPlayerLeave().pipe(
//             map(({id, player}) =>
//                 tournamentUpdateJudge(PLAYER_REMOVE, {id, player}),
//             ),
//         ),
//     )),
// );
//
// export const updateQueueStreamEpic = (action$) => action$.pipe(
//     ofType(QUEUE_UPDATE),
//     tap(el => console.log('inited_queue')),
//     switchMap(() => race(
//         socket.onJoinQueue().pipe(
//             tap(() => console.log('init join')),
//             map((player) =>
//                 tournamentUpdatePlayers(QUEUE_JOIN, player),
//             ),
//         ),
//         socket.onLeaveQueue().pipe(
//             map((player) =>
//                 tournamentUpdateQueue(QUEUE_LEAVE, player),
//             ),
//         )),
//     ),
// );

const createRoundes = (matches) => (
    matches
        .map(({roundNumber}) => roundNumber)
        .filter((x, i, a) => a.indexOf(x) == i)
        .map(el => ({
            id: uuid(),
            roundNumber: el,
            matches: matches.filter(item => item.roundNumber === el)
        }))
);

export const tournamentReducer = (state = {
    queueActive: false,

    selectedMatch: {players: []},
    roundes: [],
    judges: [],
    queue: [],
    players: [],
}, action) => {
    switch (action.type) {

        case LOAD_SUCCESS:
            console.log(action.payload);
            return {
                ...state,
                players: action.payload.players,
                matches: action.payload.matches,
                queue: action.payload.players.filter(item => item.inqQueue),
                roundes: createRoundes(action.payload.matches),
            };
        case QUEUE_JOIN:
            console.log(action.payload);
            return {...state, queue: [...state.queue, action.payload]};
        /* case UPDATE:
             return {
                 ...state,
                 tournament: {status: action.payload}
             };*/
        // case MATCHES_ADD:
        //     return {...state, roundes: [action.payload]};

        // case PLAYERS_UPDATE:
        //     return {...state, players: action.payload};
        // case PLAYER_ADD:
        //     return {...state, players: [...state.players, action.payload]};
        // case PLAYER_REMOVE:
        //     return {...state, players: [...state.players.filter(({id}) => id === action.payload.id)]};
        // case JUDGES_UPDATE:
        //     return {...state, judges: action.payload};
        // case JUDGE_ADD:
        //     return {...state, judges: [action.payload]};
        // case JUDGE_REMOVE:
        //     return {...state, judges: [...state.judges.filter(({id}) => id === action.payload.id)]};

        default:
            return state;
    }
};
// switchMap(() => race(
//     socket.onUpdateTournament().pipe(
//         tap((data)=>console.log(data))
//     ))
