import {ofType} from 'redux-observable';
import {ajax} from 'rxjs/ajax';
import {map, switchMap, tap} from 'rxjs/operators';
import {BACKEND_URL} from '../api/ajax';
import {projectRoot} from './root';

const apiURL = `${BACKEND_URL}/tournament`;
const app = 'tournament';
const MATCH_SELECT = `${projectRoot}/${app}/match/select`;
const MATCH_SELECT_SUCCESS = `${projectRoot}/${app}/match/select/success`;
const MATCH_CLOSE = `${projectRoot}/${app}/match/close`;

export const matchSelect = (match) => ({type: MATCH_SELECT, payload: match});
export const matchSelectSuccess = (match) => ({type: MATCH_SELECT_SUCCESS, payload: match});
export const closeMatch = () => ({type: MATCH_CLOSE});

export const matchEpic = (action$) => action$.pipe(
    ofType(MATCH_SELECT),
    tap(({payload}) => console.log(payload)),
    switchMap(({payload}) =>
        ajax.getJSON(BACKEND_URL + '/match/' + payload.id).pipe(
            map((data) => matchSelectSuccess(data)),
        )
    )
);

export const matchReducer = (state = {activeSelector: false, players: []}, action) => {
    switch (action.type) {
        case MATCH_CLOSE:
            return {...state, players:[],activeSelector:false};
        case MATCH_SELECT_SUCCESS:
            console.log(action.payload);
            return {...state, players: action.payload.players, activeSelector: true};
        default:
            return state;
    }

};
