import { ofType } from 'redux-observable';
import { EMPTY } from 'rxjs';
import { map, mapTo, mergeMap, switchMap, takeUntil, tap } from 'rxjs/operators';
import { uuid } from '../api/uuid';
import { projectRoot } from './root';
const app = 'notifications';

const initialState = {
    notifications: [],
};

export const NOTIFICATION_RECEIVE = `${projectRoot}/${app}/receive`;
export const NOTIFICATION_RECEIVE_SUCCESS = `${projectRoot}/${app}/receive/success`;
export const NOTIFICATION_REMOVE_SUCCESS = `${projectRoot}/${app}/remove/success`;

export const receiveNotification = (message) => ({ type: NOTIFICATION_RECEIVE, payload: message });
export const removeNotifcation = (id) => ({ type: NOTIFICATION_REMOVE_SUCCESS, payload: id });
export const receiveNotificationSuccess = (notification) => ({ type: NOTIFICATION_RECEIVE_SUCCESS, payload: notification });

export const notificationEpic = (action$) => action$.pipe(
    ofType(NOTIFICATION_RECEIVE),
    map(({ payload }) => ({ payload: { id: uuid(), message: payload } })),
    mapTo(({ payload }) => receiveNotificationSuccess(payload)),
);

export const notificationReducer = (state = initialState, action) => {
    switch (action.type) {
        case NOTIFICATION_RECEIVE_SUCCESS:
            return { ...state, notifications: [...state.notifications, ...action.payload] };
        case NOTIFICATION_REMOVE_SUCCESS:
            return { ...state, notifications: state.notifications.filter(({ id }) => id !== action.payload) };
        default:
            return state;
    }
};
