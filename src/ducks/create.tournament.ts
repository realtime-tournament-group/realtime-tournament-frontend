import { ActionsObservable, Epic, ofType } from 'redux-observable';
import { of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { catchError, debounceTime, map, mapTo, switchMap, tap } from 'rxjs/operators';
import { BACKEND_URL, HEADERS } from '../api/ajax';
import { db } from '../api/db';
import { projectRoot } from './root';
const app = 'create_tournament';

export const CREATE_TOURNAMENT = `${projectRoot}/${app}/create`;
export const CREATE_TOURNAMENT_SUCCESS = `${projectRoot}/${app}/success`;
export const CREATE_TOURNAMENT_FAILURE = `${projectRoot}/${app}/success`;
export const SAVE_TOURNAMENT_HASH = `${projectRoot}/${app}/save_hash`;
export const SAVE_TOURNAMENT_HASH_SUCCESS = `${projectRoot}/${app}/save_hash_success`;
export const SAVE_TOURNAMENT_HASH_FAILURE = `${projectRoot}/${app}/save_hash_failure`;

const tournamentApiURL = `${BACKEND_URL}/tournament`;
interface ICreateTournamentAction {
    type: typeof CREATE_TOURNAMENT;
    tournament: { name: string, status: number, open: boolean };
}
export const createTournament = (name: string, status: number, isOpen: boolean): ICreateTournamentAction => ({
    type: CREATE_TOURNAMENT,
    tournament: { name, status, open: isOpen },
});
export const saveTournament = (name: string, hash: string) => ({
    type: SAVE_TOURNAMENT_HASH,
    name,
    hash,
});
export const saveTournamentSuccess = (name: string) => ({
    type: SAVE_TOURNAMENT_HASH_SUCCESS,
    tournament: { name, status: true },
});
export const createTournamentEpic = (action$) => action$.pipe(
    ofType(CREATE_TOURNAMENT),
    debounceTime(1000),
    switchMap(({ tournament }) => ajax({
        url: tournamentApiURL,
        body: JSON.stringify(tournament),
        headers: HEADERS,
        method: 'POST',
    }).pipe(
        map((resp) => resp.response),
        map(({ name, privateHash }) => saveTournament(privateHash, name)),
        catchError((error) => of({ type: SAVE_TOURNAMENT_HASH_FAILURE })),
    )),

);

export const saveTournamentEpic = (action$) => action$.pipe(
    ofType(SAVE_TOURNAMENT_HASH),
    tap(({ hash, name }) => { localStorage.setItem(hash, name); }),
    map(({ hash }) => saveTournamentSuccess(hash)),
    catchError((err) => of({ type: SAVE_TOURNAMENT_HASH_FAILURE })),
);

export const createTournamentReducer = (state = { tournament: { status: false } }, action) => {
    switch (action.type) {
        case SAVE_TOURNAMENT_HASH_SUCCESS:
            return { state, tournament: action.tournament };
        default:
            return state;

    }
};
