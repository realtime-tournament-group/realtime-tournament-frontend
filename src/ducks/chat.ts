import {ofType} from 'redux-observable';
import {
    debounceTime,
    map,
    mapTo,
    mergeMap,
    race,
    startWith,
    switchMap,
    take,
    takeUntil,
    takeWhile,
    tap,
    switchMapTo,
    finalize
} from 'rxjs/operators';
import {uuid} from '../api/uuid';
import {IMessageContext} from '../components/Dashboard/Chat/types/message.context';
import ChatSocket from '../sockets/chat';
import {projectRoot} from './root';
import {of} from 'rxjs';

export const chatSocket = new ChatSocket();
const app = 'chat';
export const CONNECT = `${projectRoot}/${app}/connect`;
export const CONNECTION_SUCCESS = `${projectRoot}/${app}/connection_success`;
export const CONNECTION_FAILURE = `${projectRoot}/${app}/connection_failure`;
export const RECEIVE_MESSAGE = `${projectRoot}/${app}/receive_message`;
export const RECEIVE_MESSAGE_STREAM_ENABLE = `${projectRoot}/${app}/receive_message_stream_enable`;
export const RECEIVE_MESSAGE_STREAM_DISABLE = `${projectRoot}/${app}/receive_message_stream_disable`;
export const SEND_MESSAGE = `${projectRoot}/${app}/send_message`;
export const SEND_MESSAGE_SUCCESS = `${projectRoot}/${app}/send_message_success`;

export const connectToSocketChat = (name) => ({type: CONNECT, payload: name});
export const connectionSuccess = () => ({type: CONNECTION_SUCCESS});
export const connectionFailure = () => ({type: CONNECTION_FAILURE});
export const sendMessage = (message) => ({
    type: SEND_MESSAGE,
    context: message,
});
export const sendMessageSuccess = () => ({
    type: SEND_MESSAGE_SUCCESS,
});
export const recieveMessage = (message) => ({
    type: RECEIVE_MESSAGE,
    payload: message,
});
export const startMessageStream = () => ({type: RECEIVE_MESSAGE_STREAM_ENABLE});
export const chatConnectEpic = (action$) => action$.pipe(
    ofType(CONNECT),
    tap(({payload}) => chatSocket.connect(payload)),
    mapTo(startMessageStream()),
);
export const receiveMessageStreamEpic = (action$) => action$.pipe(
    ofType(RECEIVE_MESSAGE_STREAM_ENABLE),
    switchMap((action) =>
        chatSocket.onMessage()
            .pipe(
                tap((message) => console.log(message)),
                map(({name, message}) => ({
                    author: {
                        name,
                        hash: uuid(),
                        color: '#000',
                        //TODO: добавить рандомные цвета
                    },
                    message: {
                        id: uuid(),
                        text: message,
                    },
                })),
                map((value) => recieveMessage(value)),
            ),
    ),
);
export const sendMessageEpic = (action$, store) => action$.pipe(
    ofType(SEND_MESSAGE),
    map(({context}) => ({context, nick: localStorage.getItem('nickname')|| 'player_'})),
    tap(({context, nick}) => chatSocket.sendMessage(context, nick)),
    mapTo(sendMessageSuccess()),
);

export const chatReducer = (state = {messages: []}, action) => {

    switch (action.type) {
        case RECEIVE_MESSAGE:
            return {...state, messages: [...state.messages, action.payload]};
        default:
            return state;
    }
};
