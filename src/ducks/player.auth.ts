import {ActionsObservable, Epic, ofType} from 'redux-observable';
import {iif, of, throwError} from 'rxjs';
import {ajax} from 'rxjs/ajax';
import {catchError, debounceTime, defaultIfEmpty, map, mapTo, switchMap, takeWhile, tap} from 'rxjs/operators';
import {BACKEND_URL, HEADERS} from '../api/ajax';
import {projectRoot} from './root';

const app = 'player_auth';
const tournamentApiURL = `${BACKEND_URL}/tournament/realtime`;

export const AUTH = `${projectRoot}/${app}/auth`;
export const AUTH_CHECK = `${projectRoot}/${app}/auth/check`;
export const AUTH_CHECK_STATUS = `${projectRoot}/${app}/auth/check/status`;
export const AUTH_CHECK_FAILURE = `${projectRoot}/${app}/auth/check/status`;

export const AUTH_SUCCESS = `${projectRoot}/${app}/auth/success`;
export const AUTH_FAILURE = `${projectRoot}/${app}/auth/failure`;

export const authPlayer = (name, tournamentName) => ({type: AUTH, payload: {name, tournamentName}});
export const authPlayerCheck = (tournamentName) => ({type: AUTH_CHECK, payload: tournamentName});

export const authPlayerCheckStatus = (status) => ({type: AUTH_CHECK_STATUS, payload: status});
export const authPlayerSuccess = () => ({type: AUTH_SUCCESS});
export const authCheckFailure = () => ({type: AUTH_CHECK_FAILURE});

export const authPlayerFailure = () => ({type: AUTH_FAILURE});

export const playerAuthCheckEpic = (action$) => action$.pipe(
    ofType(AUTH_CHECK),
    map(({payload}) => ({
        hash: localStorage.getItem('player') || 'null',
        tournamentName: payload,
    })),
    tap((data) => console.log(data)),
    switchMap((data) => ajax.post(tournamentApiURL + '/connect/check', data, HEADERS).pipe(
        map(({response}) => response),
        map(({status}) => status ? authPlayerSuccess() : authCheckFailure())
        )
    )
);

export const playerAuthEpic = (action$) => action$.pipe(
    ofType(AUTH),
    tap((data) => console.log(data)),

    switchMap(({payload}) => ajax.post(tournamentApiURL + '/connect/user', payload, HEADERS)
        .pipe(
            map(({response}) => response),
            map(({status}) => localStorage.setItem('player', status)),
            tap(() => localStorage.setItem('nickname', payload.name)),
            map(() => authPlayerCheck(payload.tournamentName)),
        ),
    )
    )
;

export const playerAuthReducer = (state = {
    data: [],
    status: true,
    redirect: false,
    authStatus: false
}, action) => {

    switch (action.type) {
        case AUTH_CHECK_FAILURE:

            return {
                ...state,
                status: true,
                redirect: false,
                authStatus: false,
            };
        case AUTH_SUCCESS:
            console.log(action);
            return {
                ...state,
                status: false,
                redirect: false,
                authStatus: action.payload,
            };
        case AUTH_FAILURE:
            console.log(action);
            return {
                ...state,
                status: true,
                redirect: true,
                authStatus: true,
            };

        default:
            return state;
    }
};
