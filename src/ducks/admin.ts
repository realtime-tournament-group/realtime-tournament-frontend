import {ofType} from 'redux-observable';
import {iif, of} from 'rxjs';
import {ajax} from 'rxjs/ajax';
import {catchError, debounceTime, flatMap, map, mapTo, mergeMap, switchMap, takeWhile, tap} from 'rxjs/operators';
import {BACKEND_URL, HEADERS} from '../api/ajax';
import {db} from '../api/db';
import {closeMatch} from './match';
import {projectRoot} from './root';

const apiUrl = BACKEND_URL + '/tournament';
const app = 'admin';

const ADMIN_CHECK = `${projectRoot}/${app}/check`;
const ADMIN_CHECK_SUCCESS = `${projectRoot}/${app}/check_success`;
const ADMIN_CHECK_FAILURE = `${projectRoot}/${app}/check_failure`;
const SET_SCORE = `${projectRoot}/${app}/set/score`;
const SET_SCORE_SUCCESS = `${projectRoot}/${app}/set/score/success`;

const START_TOURNAMENT = `${projectRoot}/${app}/start_tournament`;
const START_TOURNAMENT_SUCCESS = `${projectRoot}/${app}/start_tournament_success`;
const START_TOURNAMENT_FAILURE = `${projectRoot}/${app}/start_tournament_failure`;
const SET_JUGDE = `${projectRoot}/${app}/set_judge`;
const SET_JUGDE_SUCCESS = `${projectRoot}/${app}/set_judge_success`;
const SET_JUGDE_FAILURE = `${projectRoot}/${app}/set_judge_failure`;
const REMOVE_JUGDE = `${projectRoot}/${app}/remove_judge`;
const REMOVE_JUGDE_SUCCESS = `${projectRoot}/${app}/remove_judge_success`;
const REMOVE_JUGDE_FAILURE = `${projectRoot}/${app}/remove_judge_failure`;

export const adminCheck = (name) => ({type: ADMIN_CHECK, tournamentName: name});
const adminCheckStatus = (status) => ({type: ADMIN_CHECK_SUCCESS, status});

export const startTournament = (name) => ({type: START_TOURNAMENT, name});
const startTournamentSuccess = () => ({type: START_TOURNAMENT_SUCCESS});
const startTournamentFailure = () => ({type: START_TOURNAMENT_FAILURE});
export const setScore = (data) => ({type: SET_SCORE, payload: data});

const setScoreSuccess = () => ({type: SET_SCORE_SUCCESS});
const setJudge = (judge) => ({type: SET_JUGDE, judge});
const setJudgeSuccess = () => ({type: SET_JUGDE_SUCCESS});
const setJudgeFailure = () => ({type: SET_JUGDE_FAILURE});

const removeJudge = (judge) => ({type: REMOVE_JUGDE, judge});
const removeJudgeSuccess = () => ({type: REMOVE_JUGDE_SUCCESS});
const removeJudgeFailure = () => ({type: REMOVE_JUGDE_FAILURE});

export const adminCheckEpic = (action$) => action$.pipe(
    ofType(ADMIN_CHECK),

    map(({tournamentName}) => localStorage.getItem(tournamentName)),
    switchMap((hash) => ajax.post(apiUrl + '/check', {hash}, HEADERS).pipe(
        map((resp) => resp.response),
        tap((resp) => console.log(resp)),
        tap(() => localStorage.setItem('nickname', 'admin')),
        map(({status}) => adminCheckStatus(status)))));

export const startTournamentEpic = (action$, state$) => action$.pipe(
    ofType(START_TOURNAMENT),
    map(({name}) => ({hash: localStorage.getItem(name)})),
    switchMap(({hash}) =>
        ajax.post(apiUrl + '/realtime/start', {privateHash: hash}, HEADERS)
            .pipe(
                mapTo(startTournamentSuccess()),
            ),
    ),
);
export const setScoreEpic = (action$) => action$.pipe(
    ofType(SET_SCORE),
    debounceTime(500),
    // @ts-ignore
    tap(({payload}) => console.log(payload)),
    map(({payload}) => ({player: payload.player, hash: localStorage.getItem(payload.hash), score: 1})),
    switchMap((data) =>
        ajax.post(apiUrl + '/realtime/match/set_score', data, HEADERS)
            .pipe(
                mapTo(closeMatch()),
            ),
    ),
);
// export const setJudgeEpic = (action$) => action$.pipe(
//     ofType(SET_JUGDE),
//     map(({name}) => ({name, hash: localStorage.getItem(name)})),
//
//     switchMap(({hash, name, judge}) =>
//         ajax.post(apiUrl + '/realtime/judge/add', {hash, judge}, HEADERS)
//             .pipe(
//                 mapTo(setJudgeSuccess()),
//                 // catchError((err) => of(setJudgeFailure())),
//             ),
//     ),
// );
// export const removeJudgeEpic = (action$) => action$.pipe(
//     ofType(REMOVE_JUGDE),
//     map(({judge}) => ({judge, hash: db.getHash(name)})),
//     switchMap(({hash, judge}) =>
//         ajax.post(apiUrl + '/realtime/judge', {hash, judge}, HEADERS)
//             .pipe(
//                 mapTo(removeJudgeSuccess()),
//                 // catchError((err) => of(removeJudgeFailure())),
//             ),
//     ),
// );

export const adminReducer = (state = {waiting: true, status: false}, action) => {

    switch (action.type) {
        case ADMIN_CHECK_SUCCESS:
            console.log(action.status);
            return {...state, status: action.status, waiting: false};

        default:
            return state;
    }
};
