import {ofType} from 'redux-observable';
import {iif, of} from 'rxjs';
import {ajax} from 'rxjs/ajax';
import {
    catchError,
    defaultIfEmpty,
    endWith,
    flatMap,
    map,
    mapTo,
    mergeMap,
    switchMap,
    takeWhile,
    tap
} from 'rxjs/operators';
import {BACKEND_URL, HEADERS} from '../api/ajax';
import PlayerSocket from '../sockets/player';
import {projectRoot} from './root';

const apiUrl = `${BACKEND_URL}/tournament/realtime`;
const app = 'player';


const QUEUE_JOIN = `${projectRoot}/${app}/queue/join`;
const QUEUE_JOIN_SUCCESS = `${projectRoot}/${app}/queue/join/success`;
const QUEUE_JOIN_FAILURE = `${projectRoot}/${app}/queue/join/failure`;

export const QUEUE_LEAVE = `${projectRoot}/${app}/queue/leave`;
const QUEUE_LEAVE_SUCCESS = `${projectRoot}/${app}/queue/leave/success`;
const QUEUE_LEAVE_FAILURE = `${projectRoot}/${app}/queue/leave/failure`;

export const MATCH_READY = `${projectRoot}/${app}/match/ready`;
export const MATCH_READY_SUCCESS = `${projectRoot}/${app}/match/ready/success`;

export const MATCH_APPLY = `${projectRoot}/${app}/match/apply`;
export const MATCH_APPLY_SUCCESS = `${projectRoot}/${app}/match_apply/success`;

export const MATCH_CANCEL = `${projectRoot}/${app}/match/cancel`;
export const MATCH_CANCEL_SUCCESS = `${projectRoot}/${app}/match/cancel/success`;
export const MATCH_CANCEL_FAILURE = `${projectRoot}/${app}/match/cancel/failure`;

export const matchApply = (match) => ({type: MATCH_APPLY, match});
export const matchCancel = (match) => ({type: MATCH_CANCEL, match});

export const joinQueue = (name) => ({type: QUEUE_JOIN, tournamentName: name});
export const leaveQueue = (name) => ({type: QUEUE_LEAVE, tournamentName: name});

// export const playerConnectEpic =(action$)=>action$.pipe(
//     ofType(CONNECT),
//     map()
// )
console.log(apiUrl);
export const userQueueEpic = (action$) => action$.pipe(
    ofType(QUEUE_JOIN),
    map(({tournamentName}) => ({tournamentName, hash: localStorage.getItem('player')})),
    switchMap((payload) =>
        ajax.post(apiUrl + '/queue/join', payload, HEADERS)
            .pipe(
                mapTo(({type: QUEUE_JOIN_SUCCESS})),
            )
    )
);
export const userQueueLeaveEpic = (action$) => action$.pipe(
    ofType(QUEUE_LEAVE),
    map(({tournamentName}) => ({tournamentName, hash: localStorage.getItem('player')})),
    switchMap((payload) =>
        ajax.post(apiUrl + '/queue/leave', payload)
            .pipe(
                map((resp) => ({type: QUEUE_LEAVE_SUCCESS})),
            )),
);
export const userMatchCancelEpic = (action$) => action$.pipe(
    ofType(MATCH_CANCEL),
    switchMap(({matchId, userHash}) =>
        ajax.put(apiUrl, {userHash, cancelStatus: true}).pipe(
            map((resp) => ({type: MATCH_CANCEL_SUCCESS})),
            catchError((err) => of({type: MATCH_CANCEL_FAILURE})),
        ),
    ),
);


export const userMatchApplyEpic = (action$) => action$.pipe(
    ofType(MATCH_APPLY),
    switchMap(({matchId, userHash}) =>
        ajax.put(apiUrl + 'match/' + matchId, {userHash, cancelStatus: false}).pipe(
            map((resp) => ({type: MATCH_CANCEL_SUCCESS})),
            catchError((err) => of({type: MATCH_CANCEL_FAILURE})),
        ),
    ),
);

export const playerReducer = (state = {readyMatch: {}, joinedMatch: {}, inQueue: false, inGame: false,}, action) => {
    switch (action.type) {
        case MATCH_READY_SUCCESS:
            return {...state, readyMatch: action.payload};
        case QUEUE_JOIN_SUCCESS:
            console.log('in quue');
            return {...state, inQueue: true};
        case QUEUE_LEAVE_SUCCESS:
            return {...state, inQueue: false};
        case QUEUE_JOIN_FAILURE:
            return {...state, inQueue: false};
        case MATCH_APPLY_SUCCESS:
            return {...state, inGame: true, joinedMatch: action.payload};
        case MATCH_CANCEL_SUCCESS:
            return {...state, inGame: false};
        default:
            return state;

    }
};

// socket.onApplyMatch().pipe(
//     map(({ id, player }) =>
//         tournamentUpdateMatch(TOURNAMENT_APPLY_MATCH, { id, player }),
//     ),
// ),
// socket.onCancelMatch().pipe(
//     map(({ id, player }) =>
//         tournamentUpdateMatch(TOURNAMENT_CANCEL_MATCH, { id, player }),
//     ),
// ),
