import { fromEvent } from 'rxjs';
import BaseSocket from './socket';
import { BACKEND_URL } from '../api/ajax';

export default class TournamentSocket extends BaseSocket {

    constructor() {
        super('http://localhost:4500' + '/tournament');
    }
    public onUpdateTournament() {

        return fromEvent(this.client, 'update_tournament');
    }

    public onUpdatePlayers() {
        return fromEvent(this.client, 'update_players');
    }
    public onJoinQueue() {
        return fromEvent(this.client, 'user_joined_queue');

    }
    public onLeaveQueue() {
        return fromEvent(this.client, 'user_leave_queue');

    }



    public onAddJudge() {
        return fromEvent(this.client, 'added_new_judge');
    }
    public onRemoveJudge() {
        return fromEvent(this.client, 'removed_judge');
    }
    public onPlayerJoin() {
        return fromEvent(this.client, 'player_joined');
    }
    public onPlayerLeave() {
        return fromEvent(this.client, 'player_leaved');
    }
    public OnsetScore() {
        return fromEvent(this.client, 'set_scored');
    }
}
