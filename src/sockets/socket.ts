import {fromEvent} from 'rxjs';
import * as ioClient from 'socket.io-client';
import {BACKEND_URL} from '../api/ajax';

export default abstract class BaseSocket {
    private connection: string;
    protected client: any;

    constructor(url: string) {
        this.connection = url;
    }

    public connect(name: string) {
        console.log('connect-ok');
        this.client = ioClient.connect(this.connection);
        this.client.emit('connect_room', name);
    }

    public onConnection() {
        console.log('connected');
        return fromEvent(this.client, 'user_connected');
    }

    public disconnect() {
        this.client.disconnect();
    }
}
