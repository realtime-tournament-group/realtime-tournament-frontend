import { fromEvent } from 'rxjs';
import { BACKEND_URL } from '../api/ajax';
import { IMessageContext } from '../components/Dashboard/Chat/types/message.context';
import BaseSocket from './socket';

export default class ChatSocket extends BaseSocket {

    constructor() {
        super('http://localhost:4600' + '/chat');
    }
    public onMessage() {
        console.log('on message');
        return fromEvent(this.client, 'message_receive');
    }
    public sendMessage(message: string,author:string) {
        console.log(this.client);
        this.client.emit('message_to_server', { name: author, message });
    }
    public messageList() {
        return fromEvent(this.client, 'messageListRecieve');
    }
}
