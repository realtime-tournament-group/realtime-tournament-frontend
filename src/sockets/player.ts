import {fromEvent} from 'rxjs';
import BaseSocket from './socket';
import {BACKEND_URL} from '../api/ajax';

export default class PlayerSocket extends BaseSocket {

    constructor() {
        super('http://localhost:4500' + '/tournament');
    }

    public onApplyMatch() {
        return fromEvent(this.client, 'user_applyed_match');

    }

    public onCancelMatch() {
        return fromEvent(this.client, 'user_canceled_match');

    }

    public OnsetScore() {
        return fromEvent(this.client, 'set_scored');
    }
    public onUpdateMatch() {
        return fromEvent(this.client, 'user_update_match');
    }
}
