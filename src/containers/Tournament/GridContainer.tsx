import React, {Component} from 'react';
import {connect} from 'react-redux';
import TournamentBracket from '../../components/Tournament/Bracket';
import MatchDialog from '../../components/Tournament/Bracket/Match/Dialog';
import {IMatch} from '../../components/Tournament/Bracket/Match/types/IMatch';
import {matchSelect} from '../../ducks/match';

interface IGridContainerProps {
    selectMatch: (match) => void;
    roundes: any[];
}

class GridContainer extends React.Component<IGridContainerProps> {
    constructor(props) {
        super(props);
        this.handleSelect = this.handleSelect.bind(this);
    }

    handleSelect(match) {
        console.log(match);
        this.props.selectMatch(match);
    }

    render() {
        const {roundes} = this.props;
        return (
            <TournamentBracket handleSelect={this.handleSelect} roundes={roundes}/>
        );
    }
}

const mapStateToProps = (state) =>
    ({roundes: state.tournamentReducer.roundes});
const mapDispatchToProps = (dispatch) => ({
    selectMatch: (match) => dispatch(matchSelect(match))
});
export default connect(mapStateToProps, mapDispatchToProps)(GridContainer);
