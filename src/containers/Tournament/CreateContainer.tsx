import React from 'react';
import { connect } from 'react-redux';
import { BACKEND_URL } from '../../api/ajax';
import CreateTournament from '../../components/Tournament/Form/Create';
import ShareTournament from '../../components/Tournament/Form/Share';
import { createTournament } from '../../ducks/create.tournament';
interface ICreateContainerState {
    name: string;
    judgement: boolean;
    status: boolean;
}
interface ICreateContainerProps {
    tournament: { name: string, status: boolean };
    create: (name: string, judgement: boolean, status: boolean) => void;
}

class CreateContainer extends React.Component<ICreateContainerProps, ICreateContainerState> {
    constructor(props) {
        super(props);
        this.state = { name: '', judgement: false, status: false };
        this.handleChangeJudgement = this.handleChangeJudgement.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeStatus = this.handleChangeStatus.bind(this);
        this.handleCreate = this.handleCreate.bind(this);
    }
    public handleChangeName(event) {
        this.setState({ name: event.target.value });

    }
    public handleChangeJudgement(event) {
        console.log(event.target.value);
        this.setState({ judgement: event.target.value === 'judgement' });
    }
    public handleChangeStatus(event) {
        console.log(event.target.value);
        this.setState({ status: event.target.value === 'public' });
    }
    public handleCreate() {
        const { name, judgement, status } = this.state;
        console.log(this.state);
        this.props.create(name, judgement, status);
    }
    public render() {
        console.log(this.props);
        const { status, name } = this.props.tournament;
        return (
            <React.Fragment>
                {status && status ?
                    <ShareTournament adminLink={`${BACKEND_URL}/#/game/${name}/board`} shareLink={`${BACKEND_URL}/#/game/${name}/board`} /> :
                    <CreateTournament
                        handleChangeJudgement={this.handleChangeJudgement}
                        handleChangeName={this.handleChangeName}
                        handleChangeStatus={this.handleChangeStatus}
                        handleCreate={this.handleCreate}
                    />}
            </React.Fragment>

        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    create: (name, judgement, status) => dispatch(createTournament(name, judgement, status)),
});
const mapStateToProps = (state) => ({
    tournament: state.createTournamentReducer.tournament,
});
export default connect(mapStateToProps, mapDispatchToProps)(CreateContainer);
