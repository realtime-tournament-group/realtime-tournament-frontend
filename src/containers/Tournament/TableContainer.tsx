import React, {Component} from 'react';
import {connect} from 'react-redux';
import MatchDialog from '../../components/Tournament/Bracket/Match/Dialog';
import {IMatch} from '../../components/Tournament/Bracket/Match/types/IMatch';
import TournamentTable from '../../components/Tournament/Table';
import {matchSelect} from '../../ducks/match';

interface ITableContainerProps {
    selectMatch: (match) => {};
    matches: any[];
}

class TableContainer extends React.Component<ITableContainerProps> {
    constructor(props) {
        super(props);
    }

    public render() {
        return (
            <TournamentTable
                matches={this.props.matches}
                handleSelect={this.props.selectMatch}/>
        );
    }
}

const mapStateToProps = (state) => ({
    matches: state.tournamentReducer.matches,
});
const mapDispatchToProps = (dispatch) => ({
    selectMatch: (match) => dispatch(matchSelect(match))

});
export default connect(mapStateToProps, mapDispatchToProps)(TableContainer);
