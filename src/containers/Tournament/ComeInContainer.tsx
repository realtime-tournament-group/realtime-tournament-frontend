import React from 'react';
import {Redirect} from 'react-router';
import ComeInTournament from '../../components/Tournament/Form/ComeIn';
import {connect} from 'react-redux';
import {autoCompleteRequest, comeInRequest} from '../../ducks/comeIn.tournament';

interface IComeInContainerProps {
    data: any[];
    autocomplete: () => void;
    connectRequest: (name) => void;
    redirect: boolean;
}

interface ComeInContainerState {
    value: ''
}

class ComeInContainer extends React.Component<IComeInContainerProps, ComeInContainerState> {
    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.changeValue = this.changeValue.bind(this);
        this.connect = this.connect.bind(this);
    }

    componentDidMount(): void {
        this.props.autocomplete();
    }

    public changeValue(data) {
        this.setState({value: data});
    }

    public connect(event) {
        event.preventDefault();
        console.log(this.state);
        this.props.connectRequest(this.state.value);
    }

    render() {
        const {redirect} = this.props;
        return (
            <React.Fragment>
                {redirect ? <Redirect to={`/game/player/${this.state.value}`}/> :
                    <ComeInTournament
                        connect={this.connect}
                        data={this.props.data}
                        value={this.state.value}
                        changeValue={this.changeValue}
                    />
                }

            </React.Fragment>

        );
    }
};

const mapStateToProps = (state) =>
    ({data: state.comeInReducer.data, redirect: state.comeInReducer.redirect});
const mapDispatchToProps = (dispatch) => ({
    autocomplete: () => dispatch(autoCompleteRequest()),
    connectRequest: (name) => dispatch(comeInRequest(name))

});
export default connect(mapStateToProps, mapDispatchToProps)(ComeInContainer);
