import React from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router';
import { Dispatch } from 'redux';
import { uuid } from '../../api/uuid';
import Chat from '../../components/Dashboard/Chat';
import { IMessageContext } from '../../components/Dashboard/Chat/types/message.context';
import { connectToSocketChat, sendMessage, startMessageStream} from '../../ducks/chat';

interface IChatContainerState {
    messages: any[];
    value: string;
}
interface IChatContainerProps {
    match:any;
    messages: IMessageContext[];
    connectChat:(name:string)=>void;
    send: (message: string) => void;
}

class ChatContainer extends React.Component<IChatContainerProps, IChatContainerState> {
    constructor(props) {
        super(props);

        this.handleInput = this.handleInput.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    public randomColor = () => {

        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    public handleInput() {
        const { messages } = this.props;
        const { value } = this.state;
        this.props.send(value);
    }
    public handleChange(event) {
        console.log(event.target.value);
        this.setState({ value: event.target.value });
    }
    componentDidMount(): void {
        console.log(this.props);
        this.props.connectChat(this.props.match.params.hash);

    }

    public render() {
        const { messages } = this.props;
        console.log(messages);
        return (
            <Chat
                handleClick={this.handleInput}
                handleInput={this.handleChange}
                messages={messages}
            />
        );
    }
}
const mapStateToProps = (state) => ({
    messages: state.chatReducer.messages,
});
const mapDispatchToProps = (dispatch) => ({
    send: (message) => dispatch(sendMessage(message)),
    connectChat:(name)=>(dispatch(connectToSocketChat(name)))

});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ChatContainer));
