import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect, withRouter} from 'react-router';
import TournamentQueue from '../../components/Tournament/Queue';
import {leaveQueue, QUEUE_LEAVE} from '../../ducks/player';

interface IQueueContainerProps {
    leave: (name: string) => void;
    active: boolean;
    queue: any[];
    match: any;

}

class QueueContainer extends React.Component<IQueueContainerProps> {
    constructor(props) {
        super(props);
        this.leave = this.leave.bind(this);
    }

    public leave() {
        this.props.leave(this.props.match.params.hash);
    }

    shouldComponentUpdate(nextProps: Readonly<IQueueContainerProps>, nextState: Readonly<{}>, nextContext: any): boolean {
        return nextProps.queue.length > this.props.queue.length;
    }

    public render() {
        const {active, queue} = this.props;
        return (
            <React.Fragment>
                <TournamentQueue
                    active={active}
                    count={queue.length}
                    handleLeave={this.leave}
                />
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    leave: (name: string) => dispatch(leaveQueue(name)),

});
const mapStateToProps = (state) => ({
    active: state.playerReducer.inQueue,
    queue: state.tournamentReducer.queue,
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(QueueContainer));
