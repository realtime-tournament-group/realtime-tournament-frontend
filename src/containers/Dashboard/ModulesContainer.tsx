import React from 'react';
import Modules from '../../components/Dashboard/Module';

export default class ModulesContainer extends React.Component {
  public render() {
    return (
      <Modules modules={[1, 2, 3]} />
    );
  }
}
