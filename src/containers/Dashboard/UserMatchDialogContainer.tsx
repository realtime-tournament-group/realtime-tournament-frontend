import React, {Component} from 'react';
import {connect} from 'react-redux';
import UserMatchDialog from '../../components/Dashboard/UserMatchDialog';
import {matchApply, matchCancel} from '../../ducks/player';

interface IUserMatchDialogContainerProps {
    match: any;
    apply: (match) => void;
    cancel: (match) => void;
}

class UserMatchDialogContainer extends React.Component<IUserMatchDialogContainerProps> {
    constructor(props) {
        super(props);
        this.applyMatch = this.applyMatch.bind(this);
        this.cancelMatch = this.cancelMatch.bind(this);
    }

    applyMatch() {
        this.props.apply(this.props.match);
    }

    cancelMatch() {
        this.props.cancel(this.props.match);
    }

    render() {
        const {match} = this.props;
        return (
            <UserMatchDialog
                apply={(e) => this.applyMatch()}
                cancel={(e) => this.cancelMatch()}
                match={match}/>
        );
    }
}

const mapStateToProps = (state) => ({match: state.playerReducer.readyMatch});
const mapDispatchToProps = (dispatch) => ({
    apply: (match) => dispatch(matchApply(match)),
    cancel: (match) => dispatch(matchCancel(match)),
});
export default connect(mapStateToProps, mapDispatchToProps)(UserMatchDialogContainer);
