import React from 'react';
import { connect } from 'react-redux';
import JudgeList from '../../components/Dashboard/Judge/List';
import AddJudgeModal from '../../components/Dashboard/Judge/Modal/AddJudge';

function JudgeContainer({ judges, players, removeJudge, openDialogHandle, addJudge, open, handleClose }) {
    return (
        <React.Fragment>
            <JudgeList addJudge={openDialogHandle} removeJudge={removeJudge} judges={judges} />
            <AddJudgeModal judges={players} handleAddJudge={addJudge} handleClose={handleClose} open={open} />
        </React.Fragment>
    );
}
const mapStateToProps = (state) => ({
    judges: state.judgeReducer.judges,
    players: state.playerReducer.players,
});
const mapDispatchToProps = (dispatch) => ({
    addJudge: (judgeId: number) => dispatch({ type: 'JUDGE_ADD', judgeId }),
    removeJudge: (judgeId: number) => dispatch({ type: 'JUDGE_REMOVE', judgeId }),
});
export default connect(mapStateToProps, mapDispatchToProps)(JudgeContainer);
