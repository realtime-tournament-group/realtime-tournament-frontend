import React, {Component} from 'react';
import {connect} from 'react-redux';
import Tournament from '../../components/Tournament';
import {connectToSocketChat} from '../../ducks/chat';
import {tournamentConnect} from '../../ducks/tournament';
import TournamentSocket from '../../sockets/tournament';

interface ITournamentContainerProps {
    match: any;
    queueActive: boolean;
    connect: (name: string) => void;
}

class TournamentContainer extends React.Component<ITournamentContainerProps> {
    constructor(props) {
        super(props);
    }

    componentDidMount(): void {


    }

    public render() {

        return (
            <Tournament
                match={this.props.match}
                queueActive={this.props.queueActive}
            />
        );
    }
}

const mapStateToProps = (state) => ({
    queueActive: state.playerReducer.inQueue,

});
const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(TournamentContainer);
