import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect, Route} from 'react-router-dom';
import PlayerAuth from '../../components/Auth/Player';
import Dashboard from '../../components/Dashboard';
import {joinQueue} from '../../ducks/player';
import {tournamentConnect, updateQueue} from '../../ducks/tournament';
import TournamentSocket from '../../sockets/tournament';

import TournamentContainer from './TournamentContainer';
import UsersListContainer from './UsersListContainer';

interface IPlayerDashboardContainerProps {
    match: any;
    connect: (name: string) => void;
    joinQueue: (name) => void;
    readyMatch: {};
}

class PlayerDashboardContainer extends Component<IPlayerDashboardContainerProps> {
    constructor(props) {
        super(props);
        this.join = this.join.bind(this);
    }

    componentDidMount(): void {
        this.props.connect(this.props.match.params.hash);

    }

    public playerRoutes() {
        const {match} = this.props;
        return [
            {label: 'Tournament Grid', link: `${match.url}/board`},
            {label: 'Users', link: `${match.url}/users`},
            {label: 'Stream', link: `${match.url}/stream`},

        ];
    }

    public join() {
        this.props.joinQueue(this.props.match.params.hash);
    }

    render() {
        const {match} = this.props;
        return (
            <React.Fragment>

                <Dashboard routes={this.playerRoutes()} isAdmin={true} activateTournament={this.join} >
                    <React.Fragment>
                        <Route path={`${match.path}/board`} component={TournamentContainer}/>
                        <Route path={`${match.path}/users`} component={UsersListContainer}/>
                        <Route path={`${match.path}/stream`} component={UsersListContainer}/>
                    </React.Fragment>
                </Dashboard>

            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    readyMatch: state.playerReducer.readyMatch
});
const mapDispatchToProps = (dispatch) => ({
    connect: (name) => {
        dispatch(tournamentConnect(name));
        dispatch(updateQueue());
    },
    joinQueue: (name) => dispatch(joinQueue(name))
});

export default connect(mapStateToProps, mapDispatchToProps)(PlayerDashboardContainer);
