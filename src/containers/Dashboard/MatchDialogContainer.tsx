import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import MatchDialog from '../../components/Tournament/Bracket/Match/Dialog';
import { setScore } from '../../ducks/admin';
import { closeMatch } from '../../ducks/match';

interface IMatchDialogContainerProps {
    players: any[];
    handleClose: () => void;
    setScore: (player, hash: string) => void;
    match: any;
    open: boolean;
}

class MatchDialogContainer extends React.Component<IMatchDialogContainerProps> {
    constructor(props) {
        super(props);
        this.setScore = this.setScore.bind(this);
        this.handleCloseSelected = this.handleCloseSelected.bind(this);

    }

    setScore(player) {
        console.log(player);
        this.props.setScore(player, this.props.match.params.hash);
    }

    handleCloseSelected() {

        this.props.handleClose();
    }

    public shouldComponentUpdate(nextProps) {
        return nextProps.players.length !== this.props.players.length;
    }

    render() {
        const { players } = this.props;
        return (
            <MatchDialog
                open={this.props.open}
                players={players && players}
                handleClose={this.handleCloseSelected()}
                setScore={this.setScore}
            />
        );
    }
}

const mapStateToProps = (state) => ({
    players: state.matchReducer.players,
    open: state.matchReducer.activeSelector,

});
const mapDispatchToProps = (dispatch) => ({
    handleClose: () => dispatch(closeMatch()),
    setScore: (player, hash) => dispatch(setScore({ hash, player })),

});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MatchDialogContainer));
