import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect, Route, withRouter} from 'react-router-dom';
import Dashboard from '../../components/Dashboard';
import {adminCheck, startTournament} from '../../ducks/admin';
import {tournamentConnect} from '../../ducks/tournament';
import AdminPage from '../../layouts/Tournament/AdminPage';
import TournamentSocket from '../../sockets/tournament';
import ModulesContainer from './ModulesContainer';
import SettingsContainer from './SettingsContainer';
import TournamentContainer from './TournamentContainer';
import UsersListContainer from './UsersListContainer';

interface IAdminDashboardContainerProps {
    check: (name: string) => void;
    start: (name: string) => void;
    connect: (name: string) => void;
    match: any;
    waiting: boolean;
    status: boolean;

}

class AdminDashboardContainer extends React.Component<IAdminDashboardContainerProps> {
    constructor(props) {
        super(props);
        this.activateTournament = this.activateTournament.bind(this);
    }

    public activateTournament() {
        const {match} = this.props;
        this.props.start(match.params.hash);
    }

    public componentDidMount() {
        this.props.check(this.props.match.params.hash);

    }

    public componentDidUpdate(prevProps: Readonly<IAdminDashboardContainerProps>, prevState: Readonly<{}>, snapshot?: any): void {
        this.props.connect(this.props.match.params.hash);
    }

    public adminRoutes() {
        const {match} = this.props;
        return [
            {label: 'Tournament Grid', link: `${match.url}/board`},
            {label: 'Users', link: `${match.url}/users`},
            {label: 'Stream', link: `${match.url}/stream`},
            {label: 'Modules', link: `${match.url}/modules`},
            {label: 'Settings', link: `${match.url}/settings`},

        ];
    }

    public render() {
        const {status, waiting, match, children, start} = this.props;
        console.log(status && waiting);
        return (

            <React.Fragment>
                {(!waiting && status) ?
                    <Dashboard routes={this.adminRoutes()}
                               isAdmin={status}
                               activateTournament={this.activateTournament}>
                        <React.Fragment>
                            <Route path={`${match.path}/board`} component={TournamentContainer}/>
                            <Route path={`${match.path}/users`} component={UsersListContainer}/>
                            <Route path={`${match.path}/modules`} component={ModulesContainer}/>
                            <Route path={`${match.path}/settings`} component={SettingsContainer}/>
                        </React.Fragment>
                    </Dashboard> :
                    <p>un authorized</p>}
            </React.Fragment>

        );
    }
}

export const mapStateToProps = (state) => ({
    waiting: state.adminReducer.waiting,
    status: state.adminReducer.status,
});
export const mapDispatchToProps = (dispatch) => ({
    check: (name) => dispatch(adminCheck(name)),
    start: (name) => dispatch(startTournament(name)),
    connect: (name) => dispatch(tournamentConnect(name)),
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AdminDashboardContainer));
