import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect, withRouter} from 'react-router';
import PlayerAuth from '../../components/Auth/Player';
import {authPlayer, authPlayerCheck, authPlayerFailure} from '../../ducks/player.auth';

interface IPlayerAuthContainerProps {
    open: boolean;
    children: React.ReactChild;
    authorize: (name: string, tournamentName: string) => void;
    checkUser: (tournamentName: string)=>void;
    cancel: () => void;
    redirect: boolean;
    match: any;

}

interface IPlayerAuthContainerState {
    value: string
}

class PlayerAuthContainer extends React.Component<IPlayerAuthContainerProps, IPlayerAuthContainerState> {
    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.handleChange = this.handleChange.bind(this);
        this.handleApply = this.handleApply.bind(this);
        this.handleClose = this.handleClose.bind(this);

    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleApply() {
        console.log('apply');
        this.props.authorize(this.state.value,this.props.match.params.hash);
    }
    componentDidMount(): void {
        console.log(this.props.match);
        this.props.checkUser(this.props.match.params.hash);
    }

    handleClose() {
        this.props.cancel();

    }

    render() {
        const {children, open, redirect} = this.props;
        return (
            <React.Fragment>
                {redirect ? <Redirect to={'/'}/> :
                    <>
                        <PlayerAuth open={open} handleClose={this.handleClose} handleApply={this.handleApply}
                                    handleChange={this.handleChange}
                                    value={this.state.value}/>
                        {children}
                    </>
                }
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    open: state.playerAuthReducer.status,
    redirect: state.playerAuthReducer.redirect,

});

const mapDispatchToProps = (dispatch) =>
    ({
        authorize: (name: string, tournamentName: string) => dispatch(authPlayer(name, tournamentName)),
        checkUser: (tournamentName: string) => dispatch(authPlayerCheck(tournamentName)),
        cancel: () => dispatch(authPlayerFailure())
    });

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PlayerAuthContainer));

