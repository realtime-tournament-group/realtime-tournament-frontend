import React, {Component} from 'react';
import {connect} from 'react-redux';
import UsersList from '../../components/Dashboard/UsersList';

interface UsersListContainerProps {
    players: any[];
}

class UsersListContainer extends React.Component<UsersListContainerProps> {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <UsersList users={this.props.players}/>
        );
    }
}

const mapStateToProps = (state) => ({
    players: state.tournamentReducer.players
});
export default connect(mapStateToProps, null)(UsersListContainer);
