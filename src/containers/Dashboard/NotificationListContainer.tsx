import React from 'react';
import { connect } from 'react-redux';
import NotificationList from '../../components/Dashboard/Notification/List';

function NotificationListContainer({ notifications, closeNotification }) {
    return (
        <NotificationList notifications={notifications} closeNotification={closeNotification} />
    );
}
const mapStateToProps = (state) =>
    ({
        notifications: state.notificationReducer.notifications,
    });
const mapDispatchToProps = (dispatch) =>
    ({ closeNotification: (id: number) => dispatch({ type: 'NOTIFICATION_CLOSE', id }) });
    
export default connect(mapStateToProps, mapDispatchToProps)(NotificationListContainer);

