import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Auth from '../../../components/Auth';
import Navbar from '../../../components/Navbar';
import TournamentForm from '../../../components/Tournament/Form';
import ComeInContainer from '../../../containers/Tournament/ComeInContainer';
import CreateContainer from '../../../containers/Tournament/CreateContainer';
import * as ui from './index.scss';

function TournamentFormPage() {
    return (
        <div className={ui.page}>
            <Navbar />
            <div className={ui.container}>
                <TournamentForm>
                    <Switch>
                        <Route path="/tournament/create" component={() => <CreateContainer />} />
                        <Route path="/tournament/comein" component={() => <ComeInContainer />} />

                    </Switch>
                </TournamentForm>
            </div>

        </div>
    );
}

export default TournamentFormPage;
