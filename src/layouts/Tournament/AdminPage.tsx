import React, {Component} from 'react';

import AdminDashboardContainer from '../../containers/Dashboard/AdminDashboardContainer';
import MatchDialogContainer from '../../containers/Dashboard/MatchDialogContainer';

export default class AdminPage extends React.Component {
    public componentDidMount() {
    }

    public render() {

        return (
            <React.Fragment>
                <AdminDashboardContainer/>
                <MatchDialogContainer/>

            </React.Fragment>

        );
    }
}

