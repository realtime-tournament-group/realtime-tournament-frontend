import React, {Component} from 'react';
import Dashboard from '../../components/Dashboard';
import MatchDialogContainer from '../../containers/Dashboard/MatchDialogContainer';
import PlayerAuthContainer from '../../containers/Dashboard/PlayerAuthContainer';
import PlayerDashboardContainer from '../../containers/Dashboard/PlayerDashboardContainer';
import QueueContainer from '../../containers/Dashboard/QueueContainer';
import {Route} from 'react-router-dom';
import UserMatchDialogContainer from '../../containers/Dashboard/UserMatchDialogContainer';
import TournamentContainer from '../../containers/Dashboard/TournamentContainer';

// TODO:

interface IPlayerPageProps {
    match: any;
}

export default class PlayerPage extends React.Component<IPlayerPageProps> {

    public componentDidMount() {

    }

    public render() {
        const {match} = this.props;
        return (
            <PlayerAuthContainer>
                <React.Fragment>
                    <UserMatchDialogContainer/>
                    <PlayerDashboardContainer match={match}/>
                    <MatchDialogContainer/>
                </React.Fragment>
            </PlayerAuthContainer>
        );
    }
}
export const mapStateToProps = (state) => {
    return;
};
export const mapDispatchToProps = (dispatch) => ({
    check: () => dispatch()
});
