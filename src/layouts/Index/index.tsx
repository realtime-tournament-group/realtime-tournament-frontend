import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Container from '@material-ui/core/Container';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import contactPng from '../../assets/images/contact.png';
import bannerPng from '../../assets/images/welcome.png';
import xsollaPng from '../../assets/images/xsolla.png';
import Navbar from '../../components/Navbar/index';
import * as ui from './index.scss';

export default function Index() {

    return (
        <div>
            <div className={ui.banner}>
                <Navbar/>
                <Container maxWidth="md">
                    <Grid container={true} component="div">
                        <Grid item={true} md={6} component="div">
                            <Container maxWidth="xs">
                                <Typography
                                    variant="h3"
                                    align="center"
                                    className={ui.banner_text}
                                >
                                    Realtime Tournament web service
                                </Typography>
                                <Grid
                                    container={true}
                                    spacing={10}
                                    justify="space-between"
                                    component="div"

                                >
                                    <Grid className={ui.banner_item} item={true} xs={12} md={6} component="div">
                                        <Fab
                                            size="large"
                                            color="primary"
                                            variant="extended"
                                            aria-label="delete"
                                            classes={{label: ui.action_button}}
                                            href={'#/tournament/create'}
                                        >
                                            Create Tournament

                                        </Fab>
                                    </Grid>
                                    <Grid className={ui.banner_item} item={true} xs={12} md={6} component="div">
                                        <Fab
                                            size="large"
                                            color="secondary"
                                            variant="extended"
                                            aria-label="delete"
                                            classes={{label: ui.action_button}}
                                            href={'#/tournament/comein'}
                                        >
                                            Come in Tournament
                                        </Fab>
                                    </Grid>
                                </Grid>
                            </Container>
                        </Grid>
                        <Hidden xsDown={true}>
                            <Grid component="div" item={true} xs={false} md={6}>
                                <img src={bannerPng} className={ui.banner_image} alt=""/>
                            </Grid>
                        </Hidden>

                    </Grid>
                </Container>
            </div>
            <div className={ui.our_mission}>
                <Container maxWidth="lg">
                    <Typography variant="h3" align="left" className={ui.header_text}>
                        Our Mission
                    </Typography>
                </Container>

                <Container maxWidth="md">
                    <Grid component="div" container={true}>
                        <Grid component="div" item={true} md={6} xs={12}>
                            <Typography
                                className={ui.mission_text_block}
                                variant="subtitle2"
                            >
                                we want to reduce your time for competitions and tournaments
                            </Typography>
                        </Grid>
                        <Grid component="div" item={true} md={6} xs={12}>
                            <Typography variant="subtitle2"  className={ui.mission_text_block}>
                                we want to provide a service of organizing tournaments, so you can comfortably play with your friends

                            </Typography>
                        </Grid>
                    </Grid>
                </Container>
            </div>
            <div className={ui.contact_form}>
                <Container maxWidth="lg">
                    <Typography
                        variant="h4"

                        align="left"
                        className={ui.header_text}
                    >
                        Contact Form
                    </Typography>
                    <Grid component="div" container={true}>
                        <Hidden xsDown={true}>
                            <Grid item={true} xs={12} md={7}>
                                <img src={contactPng} className={ui.contact_image}/>
                            </Grid>
                        </Hidden>
                        <Grid component="div" item={true} xs={12} md={5}>
                            <Card className={ui.contact_form_card}>
                                <CardContent>
                                    <Typography variant="h5" className={ui.header_text}>
                                        Contact with US
                                    </Typography>
                                    <form>
                                        <TextField
                                            label="Name"
                                            fullWidth={true}
                                            margin="normal"
                                            variant="outlined"
                                        />
                                        <TextField
                                            label="Email"
                                            fullWidth={true}
                                            margin="normal"
                                            variant="outlined"
                                        />
                                        <TextField
                                            id="outlined-multiline-static"
                                            label="Message"
                                            multiline={true}
                                            fullWidth={true}
                                            margin="normal"
                                            variant="outlined"
                                        />
                                        <br/>
                                        <br/>
                                        <br/>
                                        <Fab

                                            size="large"
                                            color="secondary"
                                            variant="extended"
                                            classes={{label: ui.button}}

                                        >
                                            Send Feedback

                                        </Fab>
                                    </form>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Container>
            </div>
            <footer className={ui.footer}>
                <div className={ui.xsolla_container}>
                    <img src={xsollaPng} className={ui.xsolla_logo} alt=""/>
                    <Typography variant="h4" className={ui.xsolla_text}>Powered in Xsolla Summer School</Typography>
                </div>

                <Typography variant="h6" className={ui.copyright}>Made by NickGrape</Typography>
            </footer>
        </div>
    );
};
