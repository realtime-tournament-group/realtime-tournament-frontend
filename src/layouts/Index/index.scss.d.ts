declare const styles: {
  readonly "banner": string;
  readonly "banner_text": string;
  readonly "banner_item": string;
  readonly "action_button": string;
  readonly "banner_image": string;
  readonly "tournament_list": string;
  readonly "header_text": string;
  readonly "our_mission": string;
  readonly "mission_text_block": string;
  readonly "contact_form": string;
  readonly "contact_image": string;
  readonly "contact_form_card": string;
  readonly "button": string;
  readonly "footer": string;
  readonly "copyright": string;
  readonly "xsolla_container": string;
  readonly "xsolla_text": string;
  readonly "xsolla_logo": string;
};
export = styles;

