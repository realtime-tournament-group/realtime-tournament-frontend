import * as React from 'react';
import { hot } from 'react-hot-loader/root';
import { Provider } from 'react-redux';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './components/Auth/Login';
import Register from './components/Auth/Register';
import Dashboard from './components/Dashboard';
import configureStore from './configureStore';
import AdminDashboardContainer from './containers/Dashboard/AdminDashboardContainer';
import Index from './layouts/Index';
import AdminPage from './layouts/Tournament/AdminPage';
import TournamentFormPage from './layouts/Tournament/Form/';
import PlayerPage from './layouts/Tournament/PlayerPage';
import './root.scss';
function lazy(path: string) {
    return React.lazy(() => import(path));
}
const store = configureStore();
const Root: React.FC<{}> = () => {
    return (
        <Provider store={store}>
            <Router>
                <React.Suspense fallback={<div>Loading...</div>}>

                    <Switch>
                        <Route exact={true} path="/" component={Index} />
                        <Route path="/game/player/:hash" component={PlayerPage} />
                        <Route strict={true} path="/game/:hash" component={AdminPage} />
                        <Route strict={true} path="/register" component={Register} />
                        <Route strict={true} path="/login" component={Login} />
                        <Route path="/tournament/:path" component={TournamentFormPage} />

                    </Switch>
                </React.Suspense>

            </Router>
        </Provider>
    );
};
export default hot(Root);
