import { DBSchema, IDBPDatabase, openDB } from 'idb';
import { from } from 'rxjs';

interface TournamentDb extends DBSchema {
    'tournament': {
        key: string,
        value: string,
    };

}
class IndexedDb {
    private db: IDBPDatabase<TournamentDb>;

    public async init() {
        this.db = await openDB<TournamentDb>('realtime', 1, {
            upgrade(db) {
                db.createObjectStore('tournament');
            }
        });
    }
    public async insert(name, hash) {
        console.log('inited');
        this.db.add('tournament', name, hash);
    }
    public async getHash(name) {
        if (!this.db) {
            await this.init();
        }
        const model = await this.db.get('tournament', name);

        return model;
    }
}

export const db = new IndexedDb();
db.init();
