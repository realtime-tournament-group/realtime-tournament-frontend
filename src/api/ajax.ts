import { XMLHttpRequest } from 'xmlhttprequest';

export const BACKEND_URL = 'http://localhost:4000';
export const SOCKET_URL = 'http://localhost:5000';
export const HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
    'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token',
};
