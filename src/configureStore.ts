import { applyMiddleware, compose, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createEpicMiddleware } from 'redux-observable';
import thunk from 'redux-thunk';
import { rootEpic, rootReducer } from './ducks';

const epicMiddleware = createEpicMiddleware();
export default function configureStore() {
    const store = createStore(rootReducer, composeWithDevTools(
        applyMiddleware(epicMiddleware),
    ));
    epicMiddleware.run(rootEpic);
    return store;
}
