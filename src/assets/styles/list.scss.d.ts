declare const styles: {
  readonly "container": string;
  readonly "text": string;
  readonly "avatar": string;
  readonly "button": string;
  readonly "field": string;
  readonly "list": string;
  readonly "list_container": string;
};
export = styles;

