import Button from '@material-ui/core/Button';
import * as React from 'react';
import * as ui from './index.scss';
import Logo from './Logo';

const Navbar = () => {
    return (
        <nav className={ui.navbar}>

            <Logo isDark={false} withText={true} />
            <div className={ui.action_button_container}>
                <Button classes={{ label: ui.button }} href="#/register">
                    Register
                </Button>
                <Button classes={{ label: ui.button }} href="#/login">
                    Login
                </Button>
            </div>
        </nav>
    );
};
export default Navbar;
