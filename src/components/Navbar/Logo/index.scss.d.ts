declare const styles: {
  readonly "logo_container": string;
  readonly "logo": string;
  readonly "logo_text": string;
  readonly "logo_text_dark": string;
  readonly "link": string;
};
export = styles;

