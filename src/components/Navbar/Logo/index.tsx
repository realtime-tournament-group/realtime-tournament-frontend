import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import * as React from 'react';
import { Link } from 'react-router-dom';
import logoSvg from '../../../assets/images/logo.svg';
import logo_lightSvg from '../../../assets/images/logo_light.svg';
import * as ui from './index.scss';

const Logo = ({ isDark, withText }) => {
    return (<Link to="/" className={ui.link}>
        <div className={ui.logo_container}>

            <motion.div
                animate={{
                    scale: [1, 1, 1, 1, 1],
                    rotate: [0, 0, 180, 180, 0],
                    borderRadius: ['20%', '20%', '50%', '50%', '20%'],
                }}
                transition={{
                    duration: 2,
                    ease: 'easeInOut',
                    times: [0, 0.2, 0.5, 0.8, 1],
                    loop: Infinity,
                    repeatDelay: 1,
                }}
            >
                {isDark ?
                    (<img src={logo_lightSvg} alt="" className={ui.logo} />) : (
                        <img src={logoSvg} alt="" className={ui.logo} />)}
            </motion.div>
            {withText ? (
                <Typography variant="h6" className={isDark ? ui.logo_text_dark : ui.logo_text}>
                    Realtime Tournament
                </Typography>) : ''}

        </div>
    </Link>

    );
};

export default Logo;
