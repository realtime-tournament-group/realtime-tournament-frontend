declare const styles: {
  readonly "flex_container": string;
  readonly "navbar": string;
  readonly "action_button_container": string;
  readonly "button": string;
};
export = styles;

