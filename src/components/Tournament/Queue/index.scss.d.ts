declare const styles: {
  readonly "queue": string;
  readonly "header": string;
  readonly "timerText": string;
  readonly "button": string;
};
export = styles;

