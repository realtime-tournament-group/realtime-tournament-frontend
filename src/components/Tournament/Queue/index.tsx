import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import React, { Component } from 'react';
import Logo from '../../Navbar/Logo';
import * as ui from './index.scss';

interface ITimer {
    seconds: number;
    minutes: number;
}

interface ITournamentQueueProps {
    active: boolean;
    count: number;
    handleLeave: () => void;

}

export default class TournamentQueue extends Component<ITournamentQueueProps, { timer: ITimer }> {
    constructor(props) {
        super(props);
        this.state = { timer: { seconds: 0, minutes: 0 } };
    }
    public componentDidMount() {
        this.startTimer();
    }

    public startTimer() {
        setInterval(() => this.timerInterval(), 1000);
    }
    public timerInterval() {
        const { timer } = this.state;
        let seconds = timer.seconds;
        let minutes = timer.minutes;
        if (seconds >= 59) {
            seconds = 0;
            minutes++;
            this.setState({ timer: { seconds, minutes } });
        } else {
            seconds++;
            this.setState({ timer: { seconds, minutes } });
        }
    }
    public stopTimer() {
        clearInterval();
    }

    public render() {
        const { timer } = this.state;
        const { count, handleLeave } = this.props;
        return (
            <section className={ui.queue}>
                <Typography variant="h5" className={ui.header}>
                    TournamentQueue
                </Typography>
                <Typography variant="subtitle1" className={ui.timerText}>
                    In Search {timer.minutes}:{timer.seconds}
                </Typography>
                <Logo isDark={false} withText={false} />

                <Typography variant="subtitle1" className={ui.timerText}>
                    Players in search .... {count}
                </Typography>
                <Button variant="outlined" size="large" classes={{ label: ui.button }} onClick={handleLeave} >
                    Leave search
      </Button>

            </section>

        );
    }
}
