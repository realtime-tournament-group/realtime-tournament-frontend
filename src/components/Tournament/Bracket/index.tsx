import React from 'react';
import MatchDialog from './Match/Dialog';
import Round from './Round';
import * as ui from './index.scss';

function preRender(roundes: any[]) {
    const count = roundes.map(round => round.matches.length).reverse();
    return roundes.map((round, idx) => ({...round, roundNumber: count[idx]}));
}

export default function TournamentBracket({roundes, handleSelect}) {
    const roundList = preRender(roundes ? roundes : []);
    return (
        <React.Fragment>
            <div className={ui.tournament}>
                {roundList && roundList.map(round =>
                    <Round
                        handleSelect={handleSelect}
                        key={round.id}
                        matches={round.matches}
                        roundNumber={round.roundNumber}/>)}
            </div>

        </React.Fragment>
    );
}
