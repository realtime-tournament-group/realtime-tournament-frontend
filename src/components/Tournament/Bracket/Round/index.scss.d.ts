declare const styles: {
  readonly "round": string;
  readonly "offset": string;
  readonly "spacer": string;
  readonly "square": string;
  readonly "line": string;
};
export = styles;

