import React from 'react';
import * as ui from './index.scss';

export const SpacerLine = () => (
    <React.Fragment>
        <div className={ui.spacer} />
        <div className={ui.line} />
    </React.Fragment>
);
