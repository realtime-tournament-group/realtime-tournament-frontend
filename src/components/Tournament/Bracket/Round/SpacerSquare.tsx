import React from 'react';
import * as ui from './index.scss';

export const SpacerSquare = ({ size }) => (
    <React.Fragment>
        <div className={ui.spacer} />
        <div className={ui.square} style={{ height: 100 * size }} />
    </React.Fragment>

);
