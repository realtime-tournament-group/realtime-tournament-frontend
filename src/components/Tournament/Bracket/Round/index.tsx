import React, {Component} from 'react';
import {uuid} from '../../../../api/uuid';
import Match from '../Match';
import {IMatch} from '../Match/types/IMatch';
import {IPlayer} from '../Player/types/IPlayer';
import * as ui from './index.scss';
import {SpacerLine} from './SpacerLine';
import {SpacerSquare} from './SpacerSquare';

interface IRoundProps {
    matches: IMatch[];
    roundNumber: number;
    handleSelect: (match: IMatch) => {};
}

export default class Round extends Component<IRoundProps> {
    public render() {
        const {matches, roundNumber} = this.props;
        const countSpacers = matches.length / 2;
        return (

            <React.Fragment>
                <div className={`${ui.round}`}>
                    {matches && matches.map((match) =>
                        <Match
                            handleSelect={this.props.handleSelect}
                            match={match}
                            key={match.id}
                        />)}
                    <div className={ui.spacer}/>
                </div>

                <div className={ui.offset}>
                    {matches && matches.slice(0, countSpacers).map((el) =>
                        <SpacerSquare
                            size={roundNumber}
                            key={uuid()}
                        />)}
                    <div className={ui.spacer}/>
                </div>
                <div className={ui.offset}>
                    {matches && matches.slice(0, countSpacers).map((el) => <SpacerLine key={uuid()}/>)}
                    <div className={ui.spacer}/>
                </div>
            </React.Fragment>
        );
    }
}
