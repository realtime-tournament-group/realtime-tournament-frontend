import React from 'react';
import * as ui from './index.scss';
import { IPlayer } from '../types/IPlayer';

export default function PlayerBracket({ player }: { player: IPlayer }) {
    return (
        <div className={ui.player}>
            <span>{player.name}</span>
            <span>{player.score}</span>
        </div>
    );
}
