declare const styles: {
  readonly "player": string;
  readonly "winner": string;
  readonly "loser": string;
};
export = styles;

