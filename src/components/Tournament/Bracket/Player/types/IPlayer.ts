export interface IPlayer {
    uuid:string;
    id: number;
    name: string;
    score: number;
}
