import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import {IPlayer} from '../types/IPlayer';
import * as ui from './index.scss';

export default function PlayerVersus({player, setScore}: { player: IPlayer, setScore: (player) => void }) {
    return (
        <div className={ui.player}>
            <Typography variant="h5" className={ui.name}>
                {player.name}
            </Typography>
            <Avatar alt="Remy Sharp" src="https://picsum.photos/id/994/200/300" className={ui.avatar}/>
            <Typography variant="h6" className={ui.score}>
                {player.score}
            </Typography>
            <Button variant="outlined" size="large" color="primary" classes={{label: ui.button}}
                    onClick={e => setScore(player)}>
                Select
            </Button>
        </div>
    );
}
;
