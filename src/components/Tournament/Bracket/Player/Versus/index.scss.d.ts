declare const styles: {
  readonly "player": string;
  readonly "avatar": string;
  readonly "name": string;
  readonly "score": string;
  readonly "button": string;
};
export = styles;

