import React, {Component} from 'react';
import {uuid} from '../../../../api/uuid';
import PlayerBracket from '../Player/Bracket';
import {IPlayer} from '../Player/types/IPlayer';
import MatchDialog from './Dialog';
import * as ui from './index.scss';
import {IMatch} from './types/IMatch';

interface IMatchState {
    open: boolean;

}

interface IMatchProps {
    match: IMatch;
    handleSelect: (match: IMatch) => {};
}

export default class Match extends Component<IMatchProps, IMatchState> {
    constructor(props) {
        super(props);
        this.state = {open: false};
        console.log(this.props.match);

    }

    public render() {
        const {players, status} = this.props.match;
        const {handleSelect} = this.props;
        return (
            <React.Fragment>
                <div className={ui.spacer}/>
                <div className={status ? ui.game : ui.game_end} onClick={() => handleSelect(this.props.match)}>
                    {players && players.map((player) => (<PlayerBracket key={player.id} player={player}/>))}
                </div>
            </React.Fragment>
        );
    }
}
