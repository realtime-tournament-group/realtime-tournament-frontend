import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import React from 'react';
import {uuid} from '../../../../../api/uuid';
import PlayerVersus from '../../Player/Versus';
import * as ui from './index.scss';

export default function MatchDialog({players, open, handleClose, setScore}) {
    return (
        <React.Fragment>
            <Dialog
                fullWidth={true}
                maxWidth={'md'}
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"

            >

                <DialogContent className={ui.dialog}>
                    <div className={ui.container}>
                        {players && players.map((player) =>
                            <PlayerVersus
                                key={uuid()} player={player}
                                setScore={setScore}/>)
                        }
                        <div className={ui.versus}>
                            VS
                        </div>
                    </div>

                </DialogContent>

            </Dialog>
        </React.Fragment>
    );
}
