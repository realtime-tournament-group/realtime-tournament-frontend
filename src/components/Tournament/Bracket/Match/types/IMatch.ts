import { IPlayer } from '../../Player/types/IPlayer';

export interface IMatch {

    id: number;
    status:boolean;
    players: IPlayer[];

}
