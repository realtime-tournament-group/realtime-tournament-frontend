import Paper from '@material-ui/core/Paper';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import React, {Component} from 'react';
import {Link, Route, RouteProps, Switch} from 'react-router-dom';
import {uuid} from '../../api/uuid';
import TournamentQueue from './Queue';
import GridContainer from '../../containers/Tournament/GridContainer';
import TableContainer from '../../containers/Tournament/TableContainer';
import * as ui from './index.scss';
import QueueContainer from '../../containers/Dashboard/QueueContainer';

interface ITournamentPageState {
    value: number;
    activation: boolean;
}

interface ITournamentProps {
    match: any;
    queueActive: boolean;
}

// TODO Сделать фикс табов при нажатии на определенный роут
export default class Tournament extends Component<ITournamentProps, ITournamentPageState> {
    constructor(props) {
        super(props);
        this.state = {value: 0, activation: true};
        this.handleChange = this.handleChange.bind(this);
    }

    public handleChange(event, newValue) {
        this.setState({value: newValue});
    }

    componentDidMount(): void {
        this.handleChange(null, 1);
    }

    public render() {
        const {value, activation} = this.state;
        const {match, children, queueActive} = this.props;
        return (
            <React.Fragment>
                <Paper className={ui.paper}>
                    {queueActive ?
                        <QueueContainer/> :
                        (
                            <React.Fragment>
                                <Tabs value={value} onChange={this.handleChange} aria-label="simple tabs example">

                                    <Tab label="Grid" href={`/#${match.url}/grid`}/>
                                    <Tab label="Table" href={`/#${match.url}/table`}/>
                                </Tabs>
                                <div className={ui.container}>
                                    <Switch>
                                        <Route exact={true} path={`${match.url}/grid`} component={GridContainer}/>
                                        <Route path={`${match.url}/table`} component={TableContainer}/>
                                    </Switch>
                                </div>
                            </React.Fragment>
                        )}
                </Paper>
            </React.Fragment>
        );
    }
}
