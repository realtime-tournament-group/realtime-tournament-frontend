import Paper from '@material-ui/core/Paper';
import React from 'react';
import * as ui from './index.scss';
export default function TournamentForm({ children }) {
    return (
        <Paper className={ui.paper}>
            <div className={ui.form}>
                {children}
            </div>
        </Paper>
    );

}
