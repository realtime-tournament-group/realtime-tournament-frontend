import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import deburr from 'lodash/deburr';
import React from 'react';
import Autosuggest from 'react-autosuggest';
import {uuid} from '../../../../api/uuid';
import * as ui from './index.scss';

interface IOptionType {
    name: string;
    status: boolean,
    open: boolean,
    creationDate: Date
}

function renderInputComponent(inputProps: any) {
    // tslint:disable-next-line: no-empty
    const {
        inputRef = () => {
        }, ref, ...other
    } = inputProps;

    return (
        <TextField
            fullWidth={true}
            InputProps={{
                inputRef: (node) => {
                    ref(node);
                    inputRef(node);
                },
            }}
            {...other}
        />
    );
}

function renderSuggestion(
    suggestion: IOptionType,
    {query, isHighlighted}: Autosuggest.RenderSuggestionParams,
) {
    const matches = match(suggestion.name, query);
    const parts = parse(suggestion.name, matches);
    console.log(matches);
    return (
        <MenuItem selected={isHighlighted} component="ul">
            <ListItem key={uuid()} alignItems="flex-start">
                <ListItemAvatar>
                    <Avatar src="https://image.flaticon.com/icons/svg/496/496558.svg"/>
                </ListItemAvatar>
                <ListItemText
                    primary={parts.map((part) => (
                        <span key={uuid()} style={{fontWeight: part.highlight ? 500 : 400}}>
                            {part.name}
                        </span>
                    ))}
                    secondary={
                        <React.Fragment>
                            <Typography
                                component="span"
                                variant="body2"
                                color="textPrimary"
                            >
                                {suggestion.name}
                            </Typography>

                        </React.Fragment>
                    }
                />
            </ListItem>

        </MenuItem>
    );
}

function getSuggestions(suggestions, value: string) {
    const inputValue = deburr(value.trim()).toLowerCase();
    const inputLength = inputValue.length;
    let count = 0;

    return inputLength === 0
        ? []
        : suggestions.filter((suggestion) => {
            const keep =
                count < 5 && suggestion.name.slice(0, inputLength).toLowerCase() === inputValue;

            if (keep) {
                count += 1;
            }

            return keep;
        });
}

function getSuggestionValue(suggestion: IOptionType) {
    return suggestion.name;
}

export default function Autocomplete({suggestions, changeValue,value}) {

    const [stateSuggestions, setSuggestions] = React.useState<IOptionType[]>([]);

    const handleSuggestionsFetchRequested = ({value}: any) => {
        setSuggestions(getSuggestions(suggestions, value));
    };

    const handleSuggestionsClearRequested = () => {
        setSuggestions([]);
    };

    const handleChange = () => (
        event: React.ChangeEvent<{}>,
        {newValue}: Autosuggest.ChangeEvent,
    ) => {
        console.log('change');
        changeValue(newValue);
    };

    const autosuggestProps = {
        renderInputComponent,
        handleChange,
        suggestions: stateSuggestions,
        onSuggestionsFetchRequested: handleSuggestionsFetchRequested,
        onSuggestionsClearRequested: handleSuggestionsClearRequested,
        getSuggestionValue,
        renderSuggestion,
    };

    return (
        <Autosuggest
            {...autosuggestProps}
            inputProps={{
                id: 'react-autosuggest-simple',
                label: 'TournamentName',
                placeholder: 'Please input tournament Name',
                value: value,
                onChange: handleChange(),
            }}
            theme={{
                suggestionsList: ui.list,
            }}
            renderSuggestionsContainer={(options) => (
                <Paper {...options.containerProps} className={ui.list} square={true}>
                    {options.children}
                </Paper>
            )}
        />
    );
}
