declare const styles: {
  readonly "form": string;
  readonly "text": string;
  readonly "field": string;
};
export = styles;

