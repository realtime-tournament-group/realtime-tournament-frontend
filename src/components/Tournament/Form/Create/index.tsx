import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import React from 'react';

import * as ui from './index.scss';

function CreateTournament({ handleChangeName, handleChangeJudgement, handleChangeStatus, handleCreate }) {
    return (
        <form noValidate={true} className={ui.form}>

            <Typography variant="h5" className={ui.text}>
                Create Tournament
            </Typography>

            <TextField
                className={ui.field}
                id="outlined-multiline-static"
                label="Tournament name"
                fullWidth={true}
                type="text"
                variant="outlined"
                onChange={handleChangeName}
            />
            <FormControl component="fieldset" className={ui.field} >
                <FormLabel component="legend">Tournament Judging</FormLabel>
                <RadioGroup
                    aria-label="judgement"
                    name="judge"
                    onChange={handleChangeJudgement}
                >
                    <FormControlLabel value="judgement" control={<Radio />} label="Viewers can vote" />
                    <FormControlLabel value="views" control={<Radio />} label="Judges vote" />
                </RadioGroup>
            </FormControl>
            <FormControl component="fieldset" className={ui.field} >
                <FormLabel component="legend">Tournament Publicity</FormLabel>
                <RadioGroup
                    aria-label="status"
                    name="publicity"
                    onChange={handleChangeStatus}
                >
                    <FormControlLabel value="public" control={<Radio />} label="Public" />
                    <FormControlLabel value="private" control={<Radio />} label="Private" />
                </RadioGroup>
            </FormControl>
            <Button variant="contained" size="large" color="primary" className={ui.field} onClick={handleCreate} >
                Create Tournament
        </Button>

        </form >
    );
}

export default CreateTournament;
