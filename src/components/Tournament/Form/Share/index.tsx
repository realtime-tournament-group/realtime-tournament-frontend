import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

import QRCode from 'qrcode.react';
import React from 'react';
import {
    EmailShareButton,
    FacebookIcon,
    FacebookShareButton,
    InstapaperShareButton,
    LineShareButton,
    LinkedinIcon,
    LinkedinShareButton,
    LivejournalShareButton,
    MailruShareButton,
    OKIcon,
    OKShareButton,
    PinterestIcon,
    PinterestShareButton,
    PocketShareButton,
    RedditShareButton,
    TelegramIcon,
    TelegramShareButton,
    TumblrShareButton,
    TwitterIcon,
    TwitterShareButton,
    ViberShareButton,
    VKIcon,
    VKShareButton,
    WhatsappIcon,
    WhatsappShareButton,
    WorkplaceShareButton,
} from 'react-share';
import * as ui from './index.scss';
function ShareTournament({ shareLink, adminLink }) {
    const [clipboardStatus, setStatus] = React.useState(false);
    const clipboardCopy = () => {
        setStatus(true);
        const textField = document.createElement('textarea');
        textField.innerText = shareLink;
        document.body.appendChild(textField);
        textField.select();
        document.execCommand('copy');
        textField.remove();
    };
    return (
        <div className={ui.form}>

            <Typography variant="h5" className={ui.text}>
                Tournament Share
            </Typography>
            <div className={ui.share_container}>
                <QRCode value={shareLink} size={128} renderAs="svg" />
                <div className={ui.share_social}>
                    <VKShareButton title="Realtime tournament" url={shareLink} image="https://gitlab.com/realtime-tournament-group/realtime-tournament-frontend/logo.png">
                        <VKIcon />
                    </VKShareButton>
                    <FacebookShareButton title="Realtime tournament" url={shareLink}>
                        <FacebookIcon />
                    </FacebookShareButton>
                    <TelegramShareButton title="Realtime tournament" url={shareLink}>
                        <TelegramIcon />
                    </TelegramShareButton>
                </div>
            </div>
            <Tooltip title={!clipboardStatus ? 'Copy to Clickboard' : 'Copied'} placement="top-end" onClick={clipboardCopy}>
                <code>
                    {shareLink}
                </code>
            </Tooltip>
            <Button variant="outlined" size="medium" color="primary" href={adminLink}>
                Let's go Tournament
        </Button>

        </div >
    );
}

export default ShareTournament;
