declare const styles: {
  readonly "form": string;
  readonly "text": string;
  readonly "share_container": string;
  readonly "share_social": string;
};
export = styles;

