import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import * as ui from './index.scss';
import Autocomplete from '../Autocomplete';

interface IComeInTournamentProps {
    data: any[];
    changeValue: (value: string) => void;
    value: string;
    connect:(event)=>void;
}

export default function ComeInTournament({data, changeValue, value,connect}: IComeInTournamentProps) {
    return (

        <form onSubmit={connect} noValidate={true} className={ui.form}>

            <Typography variant="h5" className={ui.text}>
                Come In Tournament
            </Typography>
            <div className={ui.autocomplete}>
                <Autocomplete suggestions={data} changeValue={changeValue} value={value}/>
            </div>
            <Button type="submit" variant="contained" size="large" color="primary" className={ui.field}>
                Come In Tournament
            </Button>

        </form>

    );
}
