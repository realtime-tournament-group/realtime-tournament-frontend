import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import React, { Component } from 'react';
import { uuid } from '../../../api/uuid';
import { IMatch } from '../Bracket/Match/types/IMatch';
import { IPlayer } from '../Bracket/Player/types/IPlayer';
import * as ui from './index.scss';
import MatchRow from './MatchRow';

interface ITournamentTableProps {
    matches: IMatch[];
    handleSelect: (match) => void;
}

const TournamentTable: React.StatelessComponent<ITournamentTableProps> = (props) => {
    const { matches, handleSelect } = props;
    return (
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell>Match Id </TableCell>
                    <TableCell align="right">Round&nbsp;(number)</TableCell>
                    <TableCell align="right">PlayerOne</TableCell>
                    <TableCell align="right">PlayerTwo</TableCell>
                    <TableCell align="right">Action</TableCell>

                </TableRow>
            </TableHead>
            <TableBody>
                {matches && matches.map((match) =>
                    <MatchRow
                        key={uuid()}
                        match={match}
                        handleSelect={handleSelect}
                    />)}
            </TableBody>
        </Table>
    );

};
export default TournamentTable;
