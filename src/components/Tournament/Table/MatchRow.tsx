import Button from '@material-ui/core/Button';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import React from 'react';
import { uuid } from '../../../api/uuid';
import { IPlayer } from '../Bracket/Player/types/IPlayer';

export default function MatchRow({ match, handleSelect }) {
    return (
        <TableRow>
            <TableCell component="th" scope="row">
                {match.id}
            </TableCell>
            <TableCell align="right">{match.round}</TableCell>
            {match.players && match.players.map((player: IPlayer) =>
                <TableCell key={uuid()} align="right">{player.name}</TableCell>)}
            <TableCell align="right">
                <Button onClick={() => handleSelect(match)}>
                    Select Match
                </Button>
            </TableCell>
        </TableRow>
    );
}
