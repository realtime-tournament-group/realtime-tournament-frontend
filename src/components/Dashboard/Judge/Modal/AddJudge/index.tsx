import Avatar from '@material-ui/core/Avatar';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import PersonIcon from '@material-ui/icons/Person';
import React from 'react';
import { uuid } from '../../../../../api/uuid';
import Judge from '../../List/Judge';

export default function AddJudgeModal({ open, handleClose, judges, handleAddJudge }) {
    return (
        <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
            <DialogTitle id="simple-dialog-title">Select Judge</DialogTitle>
            <List>
                {judges && judges.map((judge) =>
                    <Judge
                        key={uuid()}
                        judge={judge}
                        actionHandler={handleAddJudge}
                        action="Add Judge"
                    />)}
            </List>
        </Dialog>
    );

}
