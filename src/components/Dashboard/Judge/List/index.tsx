import List from '@material-ui/core/List';
import React from 'react';
import { uuid } from '../../../../api/uuid';
import * as ui from '../../../../assets/styles/list.scss';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Judge from './Judge';

function JudgeList({ judges, addJudge, removeJudge }) {
    return (
        <div className={ui.list}>
            <div className={ui.container}>
                <Typography variant="subtitle1" className={ui.text}>Judges List</Typography>
                <Button onClick={addJudge}> Add Judge</Button>
            </div>
            <List className={ui.list_container}>
                {judges && judges.map((judge) =>
                    <Judge
                        judge={judge}
                        key={judge.id}
                        actionHandler={removeJudge}
                        action="Remove"
                    />,
                )}
            </List>
        </div>
    );
}

export default JudgeList;
