import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

export default function Judge({judge, actionHandler, action}) {
    return (
        <ListItem>
            <ListItemAvatar>
                <Avatar src={judge.image}>
                </Avatar>
            </ListItemAvatar>
            <ListItemText
                primary={judge.name}
            />
            <ListItemSecondaryAction>
                <Button onClick={actionHandler}> {action} </Button>
            </ListItemSecondaryAction>
        </ListItem>
    );
}
