import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';

export default function DetailItem({detail}) {
    return (
        <ListItem>
            <ListItemAvatar>
                <Avatar src={detail.icon}>
                </Avatar>
            </ListItemAvatar>
            <ListItemText
                primary={detail.name}
            />
            <ListItemSecondaryAction>
                <Typography variant="subtitle1">{detail.data}</Typography>
            </ListItemSecondaryAction>
        </ListItem>
    );
}
