import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { uuid } from '../../../../../api/uuid';
import * as ui from '../../../../../assets/styles/list.scss';
import DetailItem from './Item';

function DetailList({ details }) {
    return (
        <div className={ui.list}>
            <div className={ui.container}>
                <Typography variant="subtitle1" className={ui.text}>Tournament Details</Typography>

            </div>
            <List className={ui.list_container}>
                {details && details.map((detail) => <DetailItem detail={detail} key={uuid()} />)}
            </List>
        </div>
    );
}

export default DetailList;
