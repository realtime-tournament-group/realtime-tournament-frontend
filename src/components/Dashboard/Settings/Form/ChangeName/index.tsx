import React from 'react';
import * as ui from '../../../../../assets/styles/list.scss';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';

import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

export default function ChangeName({value, hadleChange, confrimChange}) {
    return (
        <div className={ui.list}>
            <div className={ui.container}>

                <TextField
                    className={ui.field}
                    id="outlined-multiline-static"
                    label="Tournament Name"
                    fullWidth={true}
                    type="text"
                    margin="dense"
                    variant="outlined"
                    value={value}

                />
                <Button variant="contained" color="primary" className={ui.button} onClick={confrimChange}>
                    Change Name
                </Button>
            </div>
        </div>
    );
}
