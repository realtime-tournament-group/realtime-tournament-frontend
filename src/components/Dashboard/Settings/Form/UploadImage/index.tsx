import React from 'react';
import * as ui from '../../../../../assets/styles/list.scss';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';

import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

export default function UploadImage({logo, handleUpload}) {
    return (
        <React.Fragment>
            <div className={ui.container}>
                <Avatar alt="Remy Sharp" src={logo} className={ui.avatar}/>
                <Button color="primary" onClick={handleUpload}>
                    Upload Tournament logo
                </Button>
            </div>
        </React.Fragment>
    );
};
