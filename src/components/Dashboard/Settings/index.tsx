import * as React from 'react';
import ChangeName from './Form/ChangeName';
import UploadImage from './Form/UploadImage';
import * as ui from './index.scss';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';

import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default function Settings() {

    return (
        <Paper>

            <div className={ui.settings}>
                <Typography variant="h5" className={ui.text}>Tournament Settings</Typography>
            </div>

        </Paper>

    );

}
