import React from 'react';
import {uuid} from '../../../../api/uuid';
import Notification from './Notification';

export default function NotificationList({notifications, closeNotification}) {
    return (
        <React.Fragment>
            {notifications && notifications.map(notification =>
                <Notification
                    key={uuid()}
                    status={notification.status}
                    message={notification.message}/>)}
        </React.Fragment>
    );
}
