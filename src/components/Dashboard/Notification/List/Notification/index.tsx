import Snackbar from '@material-ui/core/Snackbar';
import * as React from 'react';

export default function Notification({message, status}) {
    const [open, setOpen] = React.useState(status);

    function handleClick() {
        setOpen(true);
    }

    function handleClose(event: React.SyntheticEvent | React.MouseEvent, reason?: string) {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    }

    return (
        <Snackbar
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            open={open}
            onClose={handleClose}
            message={<span id="message-id">{message}</span>}
        />
    );
}
