import { Button } from '@material-ui/core';
import Fab from '@material-ui/core/Fab';
import TextField from '@material-ui/core/TextField';
import Navigation from '@material-ui/icons/Navigation';
import * as React from 'react';
import * as ui from './index.scss';

export default function ChatForm({ handleClick, handleInput }) {

    return (
        <div className={ui.chat_input}>
            <TextField
                id="outlined-name"
                label="Message"
                margin="dense"
                variant="outlined"
                placeholder="Input some message"
                onChange={handleInput}
            />
            <Button color="primary" variant="contained" onClick={handleClick} >
                Send Message
            </Button>
        </div>
    );

}
