import {StatelessComponent} from 'react';
import * as React from 'react';
import * as ui from './index.scss';

import {motion} from 'framer-motion';
import ChatForm from './ChatForm';
import Message from './Message';
import {IMessageContext} from './types/message.context';

interface IChatProps {
    messages: IMessageContext[];
    handleClick: any;
    handleInput: any;
}

const Chat: StatelessComponent<IChatProps> = (props) => {
    const {messages, handleInput, handleClick} = props;
    return (

        <div className={ui.chat}>
            <div className={ui.chat_window}>
                {messages &&
                messages.map((messageCtx) => (
                    <Message
                        color={messageCtx.author.color}
                        author={messageCtx.author.name}
                        message={messageCtx.message.text}
                        key={messageCtx.message.id}
                    />))
                }
            </div>
            <ChatForm handleInput={handleInput} handleClick={handleClick}/>
        </div>

    );

};
export default Chat;
