import { IAuthor } from './author';

export interface IMessage {
    text: string;
    id: string;
}
