export interface IAuthor {
    name: string;
    hash?: string;
    color: string;
}
