import { IAuthor } from './author';

import { IMessage } from './message';

export interface IMessageContext {
    author: IAuthor;
    message: IMessage;
}
