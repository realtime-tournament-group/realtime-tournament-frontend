declare const styles: {
  readonly "message_container": string;
  readonly "message_container_other": string;
  readonly "message_author": string;
  readonly "message_container_me": string;
  readonly "message": string;
};
export = styles;

