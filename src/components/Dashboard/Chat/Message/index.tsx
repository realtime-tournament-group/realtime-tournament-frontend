import * as React from 'react';
import * as ui from './index.scss';

export default function Message({ message, author, color }) {

  return (

    <div className={ui.message_container_other}>
      <div className={ui.message_author} style={{ color }}>{author}&nbsp;:</div>
      <div className={ui.message}>
        {message}
      </div>
    </div >
  );
}
