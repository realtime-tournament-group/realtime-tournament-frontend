declare const styles: {
  readonly "dashboard_page": string;
  readonly "chat_container": string;
  readonly "absolute": string;
  readonly "drawer_open": string;
  readonly "dashboard_container": string;
  readonly "content": string;
  readonly "paper": string;
  readonly "paper_red": string;
  readonly "nav": string;
  readonly "dashboard_logo": string;
  readonly "dashboard_con": string;
};
export = styles;

