export interface ITab {
    label: string;
    path: string;
}
