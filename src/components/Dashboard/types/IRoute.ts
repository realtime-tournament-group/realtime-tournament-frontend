export interface IRoute {
    path: string;
    label: string;
    component: React.ReactNode;
}
