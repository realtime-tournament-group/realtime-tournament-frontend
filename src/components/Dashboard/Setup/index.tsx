import * as React from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import {uuid} from '../../../api/uuid';

export default function Setup({steps}) {
    return (
        <React.Fragment>
            <Stepper>
                {steps && steps.map(step =>
                    (<Step key={uuid()}><StepLabel>{step}</StepLabel></Step>)
                )}

            </Stepper>
        </React.Fragment>
    );

}
