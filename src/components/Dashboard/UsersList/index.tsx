import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { uuid } from '../../../api/uuid';
import * as ui from './index.scss';
import UsersListItem from './Item';

function UsersList({ users }) {
    return (
        <React.Fragment>
            <Typography
                variant="h6"

                color="textPrimary"
            >
                Users List
            </Typography>
            <List className={ui.list}>
                {users && users.map((user) => <UsersListItem key={uuid()} user={user} />)}
            </List>
        </React.Fragment>
    );

}

export default UsersList;
