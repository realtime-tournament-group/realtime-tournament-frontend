import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import CardModule from './Card';
import * as ui from './index.scss';

export default function Modules({ modules }: any) {
  return (
    <>
      <Typography variant="h6" >
        Modules
      </Typography>
      <div className={ui.modules}>

        {modules.map((card, idx) =>
          <CardModule key={idx} />,
        )}

      </div>
    </>
  );

}
