declare const styles: {
  readonly "card_module": string;
  readonly "card": string;
  readonly "content": string;
  readonly "card_image": string;
  readonly "title": string;
  readonly "description": string;
};
export = styles;

