import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import * as React from 'react';
import cameraSvg from '../../../../assets/images/camera.svg';
import * as ui from './index.scss';

export default function CardModule() {
    return (
        <motion.div

            whileHover={{ scale: 1.1 }}
            whileTap={{ scale: 0.9 }}
        >
            <div className={ui.card_module}>
                <Card className={ui.card}>
                    <CardContent className={ui.content}>
                        <img src={cameraSvg} className={ui.card_image} alt="" />
                        <Typography className={ui.title}>
                            WebRTC Stream
                        </Typography>
                        <Typography variant="h5" component="h2" />

                        <Typography className={ui.description} variant="body2" component="p">
                            add streaming video of tournament Webrtc protocol
                        </Typography>
                    </CardContent>

                </Card>
            </div>
        </motion.div>
    );

}
