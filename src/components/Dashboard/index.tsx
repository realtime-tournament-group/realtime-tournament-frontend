import {Divider, Drawer, List, ListItem, ListItemIcon, ListItemText, Typography, Fab} from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Paper from '@material-ui/core/Paper';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import {ThemeProvider} from '@material-ui/styles';
import * as React from 'react';
import {Link, Route, RouteProps, Switch, withRouter} from 'react-router-dom';
import {uuid} from '../../api/uuid';
import logoSvg from '../../assets/images/logo.svg';
import ChatContainer from '../../containers/Dashboard/ChatContainer';
import MatchDialogContainer from '../../containers/Dashboard/MatchDialogContainer';
import ModulesContainer from '../../containers/Dashboard/ModulesContainer';
import SettingsContainer from '../../containers/Dashboard/SettingsContainer';
import Tournament from '../Tournament';
import {theme} from '../../theme';
import * as ui from './index.scss';
import Settings from './Settings';
import {ITab} from './types/Itab';

interface IDashboardState {
    value: number;
}

interface IDashboardProps {
    children: JSX.Element;
    routes: any[];
    isAdmin: boolean;
    activateTournament: () => void;
}

class Dashboard extends React.Component<IDashboardProps, IDashboardState> {

    constructor(props) {
        super(props);
        this.state = {value: 0};
        this.handleChange = this.handleChange.bind(this);

    }

    public static defaultProps = {
        activateTournament: () => {
        },
        isAdmin: false

    };

    public handleChange(event, newValue) {
        this.setState({value: newValue});
    }

    public render() {
        const {value} = this.state;
        const {routes, children, isAdmin, activateTournament} = this.props;
        const open = true;
        return (
            <ThemeProvider theme={theme}>
                <div className={ui.dashboard_page}>
                    <AppBar position="fixed" className={ui.nav}>
                        <Toolbar variant="dense">
                            <img src={logoSvg} alt="" className={ui.dashboard_logo}/>

                            <Tabs
                                value={value}
                                onChange={this.handleChange}
                            >
                                {routes.map((tab) => <Tab key={uuid()} label={tab.label} href={'/#' + tab.link}/>)}
                            </Tabs>
                        </Toolbar>
                    </AppBar>
                    <Drawer
                        variant="persistent"
                        anchor="right"
                        open={open}
                        classes={{paper: ui.chat_container}}

                    >
                        <ChatContainer/>
                    </Drawer>

                    <section className={`${ui.dashboard_container} ${open && ui.drawer_open}`}>
                        <main className={ui.content}>
                            <Switch>
                                {children}
                            </Switch>
                        </main>
                        {isAdmin && (
                            <Tooltip title="Start tournament" aria-label="add" onClick={() => activateTournament()}>
                                <Fab color="secondary" className={ui.absolute}>
                                    +
                                </Fab>
                            </Tooltip>
                        )}

                    </section>
                </div>
            </ThemeProvider>
        );
    }

}

export default Dashboard;
