declare const styles: {
  readonly "auth_page": string;
  readonly "container": string;
  readonly "auth_card": string;
  readonly "card_content": string;
  readonly "colored_part": string;
  readonly "auth_image": string;
  readonly "auth_form": string;
};
export = styles;

