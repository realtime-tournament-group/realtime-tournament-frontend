declare const styles: {
  readonly "register_form": string;
  readonly "header_text": string;
  readonly "form_group": string;
  readonly "field": string;
  readonly "button": string;
};
export = styles;

