import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import Auth from '../';
import * as ui from './index.scss';

interface  ILoginProps {

}
class Login extends React.Component<ILoginProps> {
    public render() {
        return (
            <Auth>
                <form className={ui.register_form}>
                    <Typography variant="h4" className={ui.header_text}>Welcome Back</Typography>
                    <div className={ui.form_group}>

                        <TextField
                            className={ui.field}
                            id="outlined-multiline-static"
                            label="Email"
                            fullWidth={true}
                            type="email"
                            margin="dense"
                            variant="outlined"
                        />
                        <TextField
                            className={ui.field}
                            id="outlined-multiline-static"
                            label="Password"

                            type="password"
                            fullWidth={true}
                            margin="dense"
                            variant="outlined"
                        />
                        <Button classes={{ label: ui.button }}>
                            Sign In
                        </Button>
                    </div>
                </form>
            </Auth>
        );
    }
}

export default Login;
