import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import * as React from 'react';
import authPng from '../../assets/images/auth.png';
import Navbar from '../Navbar';
import * as ui from './index.scss';

interface AuthProp {
    children: JSX.Element | JSX.Element[];
}

const Auth: React.FC<AuthProp> = ({children}) => {
    return (

        <section className={ui.auth_page}>
            <Navbar/>
            <div className={ui.container}>
                <Card className={ui.auth_card}>
                    <CardContent className={ui.card_content}>
                        <Grid container={true}>

                            <Grid item={true} xs={12} md={6}>
                                <div className={ui.auth_form}>
                                    {children}
                                </div>
                            </Grid>
                            <Grid item={true} xs={12} md={6} className={ui.colored_part}>
                                <img src={authPng} className={ui.auth_image} alt=""/>
                            </Grid>
                        </Grid>

                    </CardContent>
                </Card>
            </div>

        </section>
    );
};

export default Auth;
