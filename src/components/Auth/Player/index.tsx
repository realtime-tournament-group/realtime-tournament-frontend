import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

interface IPlayerAuthProps {
    open: boolean;
    handleClose: () => void;
    handleApply: () => void;
    handleChange:(event)=>void;
    value:string;
}

class PlayerAuth extends React.Component<IPlayerAuthProps> {

    render() {
        const {open, handleClose,handleChange, handleApply} = this.props;
        return (
            <div>
                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Authorize Player</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please input value of PlayerName
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Player name"
                            type="text"
                            fullWidth
                            onChange={handleChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={handleApply} color="primary">
                            Apply
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default PlayerAuth;
