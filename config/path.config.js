const path = require('path');

const paths = {
    outputDir: path.resolve(__dirname, '../', 'dist/'),
    entryDir: path.resolve(__dirname, '../', 'src/index.tsx'),
    imagePath: 'images',
    htmlPath: path.resolve(__dirname, '../', 'public/index.html'),
    jsFolder: 'js',
    cssFolder: 'css',
    fontFolder: 'fonts',
};

module.exports = paths;
