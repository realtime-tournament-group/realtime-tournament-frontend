const paths = require('./path.config');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        vendor: paths.entryDir,
    },

    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.(ts|tsx)$/,
                loader: 'tslint-loader',
                exclude: /(node_modules)/,
            },
            {
                test: /\.(ts|tsx)$/,
                loader: 'ts-loader',
                exclude: /(node_modules)/,

            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: paths.imagePath,
                        },
                    },
                ],
            },
            {
                test: /\.(woff2|ttf|woff|eot)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: paths.fontFolder,
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: paths.htmlPath,

        }),
    ],
    resolve: {
        modules: ['src', 'node_modules'],
        extensions: ['*', '.ts', '.js', '.tsx', '.css', '.scss'],
    },
    output: {
        filename: '[name].js',
        path: paths.outputDir,
    },
};
