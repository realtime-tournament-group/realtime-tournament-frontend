const paths = require("./path.config");
const webpack = require("webpack");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const { TypedCssModulesPlugin } = require("typed-css-modules-webpack-plugin");
const StyleLintPlugin = require("stylelint-webpack-plugin");

module.exports = {
    mode: "development",
    output: {
        filename: "[name].js",
        path: paths.outputPath,
        chunkFilename: "[name].js",
    },
    module: {
        rules: [
            {
                test: /\.(css|scss)$/,
                use: [
                    "style-loader",

                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true,
                            importLoaders: 1,
                            modules: {
                                localIdentName: "ui-tournament_[name]__[local]--[hash:base64:5]",
                                hashPrefix: "ui-tournament",
                            },
                        },
                    },

                    "sass-loader",
                ],
            },
        ],
    },

    plugins: [
        new StyleLintPlugin({ syntax: "scss" }),
        new TypedCssModulesPlugin({
            globPattern: "src/**/*.scss",
        }),
        /*
      new BrowserSyncPlugin(

        {
          host: 'localhost',
          port: 3000,
          proxy: 'http://localhost:3100/',
        },
        {
          reload: false,
        },
      ),

         */
        new webpack.HotModuleReplacementPlugin(),
    ],
    devServer: {
        contentBase: paths.outputPath,
        host: "localhost",

        publicPath: "/",
        compress: false,
        hot: true,
        port: 5000,
        historyApiFallback: true,
        proxy: {
            'http://localhost:5000': 'http://localhost:4000',


        }
    },
    output: {
        filename: "bundle.js",
        path: paths.outputDir,
    },
};

/*

{
            loader: 'postcss-loader',
            options: {
              parser: 'postcss-scss',
              plugins: loader => [
                require('postcss-import')({ root: loader.resourcePath }),
                require('postcss-preset-env')(),
                require('postcss-url'),
                require('cssnano')(),
              ],
            },
          },
*/
